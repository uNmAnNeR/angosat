﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Angosat;

namespace AngosatTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test_C_Band_Functions()
        {
            Evaluations evs = new Evaluations();

            Angosat.Environment e = new Angosat.Environment();
            CSystem sys = new CSystem(e);

            Assert.AreEqual(35868.43, sys.Tx.d, 0.01);  // f1_1
            Assert.AreEqual(79.91, sys.Tx.Ym, 0.01);  // f1_2
            Assert.AreEqual(32.95, sys.Tx.CosPsi, 0.01); // f1_3
            Assert.AreEqual(0.37, sys.Tx.ThetaZdB, 0.01);  // f1_4
            Assert.AreEqual(54, sys.Tx.Gmax, 0.01);  // f1_5
            Assert.AreEqual(199.56, sys.Tx.Lsv, 0.01);  //f1_6
            Assert.AreEqual(0.20, sys.Tx.Theta, 0.01);  // f1_7
            Assert.AreEqual(3.70, sys.Tx.Lop, 0.01);  // f1_8
            Assert.AreEqual(3.77, sys.Tx.Ldop, 0.01);  // f1_9
            Assert.AreEqual(203.33, sys.Tx.Lmax, 0.01);  // f1_10
            Assert.AreEqual(50.29, sys.Tx.G, 0.01);  // f1_11
            // no f1_12
            Assert.AreEqual(-123.43, sys.Tx.N, 0.01);  // f1_13
            Assert.AreEqual(6.56e7, sys.Tx.delta_f, 1e5);  // f1_13NA
            Assert.AreEqual(-201.6, sys.Tx.N0, 0.01);  // f1_14
            Assert.AreEqual(81.41, sys.Tx.P, 0.01);  // f1_15
            Assert.AreEqual(-0.41, sys.Tx.OBO, 0.01);  // f1_15NA
            Assert.AreEqual(-80.67, sys.Tx.Psi, 0.01);  // f1_16
            Assert.AreEqual(65.12, sys.Tx.E, 0.01);  // f1_17
            Assert.AreEqual(-96.15, sys.Tx.C, 0.01);  // f1_17NA
            Assert.AreEqual(22.51, sys.Tx.CN, 0.01);  // f1_18
            Assert.AreEqual(100.68, sys.Tx.CN0, 0.01);  // f1_19
            Assert.AreEqual(-127.92, sys.Tx.CT, 0.01);  // f1_20
            Assert.AreEqual(21.39, sys.Tx.EbN0, 0.01);  // f1_20NA
            Assert.AreEqual(1.30, sys.Tx.Gamma, 0.01);  // f1_21NA
            Assert.AreEqual(79.91, evs.f1_22(), 0.01);
            Assert.AreEqual(32.95, evs.f1_23(), 0.01);
            Assert.AreEqual(0.56, evs.f1_24(), 0.01);
            Assert.AreEqual(50.25, evs.f1_25(), 0.01);
            Assert.AreEqual(44.52, evs.f1_26(), 0.01);
            Assert.AreEqual(195.81, evs.f1_27(), 0.01);
            Assert.AreEqual(0.20, evs.f1_28(), 0.01);
            Assert.AreEqual(1.53, evs.f1_29(), 0.01);
            Assert.AreEqual(1.60, evs.f1_30(), 0.01);
            Assert.AreEqual(197.41, evs.f1_31(), 0.01);
            Assert.AreEqual(48.72, evs.f1_32(), 0.01);
            Assert.AreEqual(1.81, evs.f1_33(), 0.01);
            Assert.AreEqual(69.02, evs.f1_33NA(), 0.01);
            Assert.AreEqual(76.52, evs.f1_34(), 0.01);
            Assert.AreEqual(226.52, evs.f1_35(), 0.01);
            Assert.AreEqual(4.86e7, evs.R_c_pr_czs(), 1e5);
            Assert.AreEqual(6.56e7, evs.f1_36NA(), 1e5);
            Assert.AreEqual(-126.88, evs.f1_36(), 0.01);
            Assert.AreEqual(-205.05, evs.f1_37(), 0.01);
            Assert.AreEqual(-107.30, evs.f1_38(), 0.01);
            Assert.AreEqual(-122.22, evs.f1_39(), 0.01);
            Assert.AreEqual(-123.60, evs.f1_40(), 0.01);
            Assert.AreEqual(26.70, evs.f1_41(), 0.01);
            Assert.AreEqual(-105.70, evs.f1_41NA(), 0.01);
            Assert.AreEqual(19.58, evs.f1_42(), 0.01);
            Assert.AreEqual(97.75, evs.f1_43(), 0.01);
            Assert.AreEqual(18.45, evs.f1_43NA(), 0.01);
            Assert.AreEqual(-130.85, evs.f1_44(), 0.01);
            Assert.AreEqual(17.79, evs.f1_45(), 0.01);
            Assert.AreEqual(95.96, evs.f1_46(), 0.01);
            Assert.AreEqual(16.67, evs.f1_46NA(), 0.01);
            Assert.AreEqual(20710.41, evs.f1_47(), 0.01);
            Assert.AreEqual(0.04, evs.f1_48(), 0.01);
            Assert.AreEqual(20531.39, evs.f1_49(), 0.01);
            Assert.AreEqual(18068.27, evs.f1_50(), 0.01);
            Assert.AreEqual(10265.69, evs.f1_51(), 0.01);
            Assert.AreEqual(2.61, evs.f1_52(), 0.01);
            Assert.AreEqual(4.18, evs.f1_53(), 0.01);
            Assert.AreEqual(8.57, evs.f1_54(), 0.01);
            Assert.AreEqual(19.07, evs.f1_55(), 0.01);
            Assert.AreEqual(1.30, evs.f1_57(), 0.01);
            Assert.AreEqual(1.6, evs.f1_56(), 0.01);
            Assert.AreEqual(1.24e-9, evs.f1_58(), 1e-10);
        }

        [TestMethod]
        public void Test_Ku_Band_Functions()
        {
            Evaluations evs = new Evaluations();
            evs.equivalent_isotropic_radiated_power = 49.29;

            evs.s_orientationCoefficient = 1.1;
            evs.s_operating_frequency = 14210;
            evs.s_antenna_diameter = 7.3;

            evs.r_orientationCoefficient = 1.3;
            evs.r_operating_frequency = 11160;
            evs.r_antenna_diameter = 7.3;
            evs.r_L_atm_pr_czs = 10 * Math.Log10(1.1);

            Angosat.Environment e = new Angosat.Environment();
            KuSystem sys = new KuSystem(e);


            // f2_1-f2_5 = f1_1-f1_5
            Assert.AreEqual(59.17, sys.Tx.Gmax, 0.01);  // f2_5
            Assert.AreEqual(13.06, sys.Tx.LA, 0.01);  // f2_6
            Assert.AreEqual(206.55, evs.f1_6(), 0.01);  // f2_7
            Assert.AreEqual(0.184, sys.Tx.Theta, 0.01);  // f2_9
            Assert.AreEqual(9.92, evs.f1_8(), 0.01);  // f2_10
            Assert.AreEqual(9.99, evs.f1_9(), 0.01);  // f2_11
            Assert.AreEqual(216.53, evs.f1_10(), 0.01);  // f2_12
            Assert.AreEqual(49.25, evs.f1_11(), 0.01);  // f2_14
            Assert.AreEqual(478.63, evs.f2_15(), 0.01);
            Assert.AreEqual(-123.63, evs.f2_16(), 0.01);      
            Assert.AreEqual(-201.8, evs.f2_17(), 0.01);
            Assert.AreEqual(-0.97, evs.f2_17NA(), 0.01);
            Assert.AreEqual(83.16, sys.Tx.P, 0.01);  // f2_18NA
            Assert.AreEqual(-78.93, evs.f2_20(), 0.01); 
            Assert.AreEqual(66.86, evs.f2_22(), 0.01);
            Assert.AreEqual(-106.38, evs.f2_23NA(), 0.01);
            Assert.AreEqual(19.16, evs.f2_24(), 0.01);
            Assert.AreEqual(-132.5, evs.f2_24NB(), 0.01);
            
            Assert.AreEqual(97.32, evs.f2_26(), 0.01);
            Assert.AreEqual(18.03, evs.f2_27NA(), 0.01);
            Assert.AreEqual(-131.28, evs.f2_28(), 0.01);
            Assert.AreEqual(-8.09, evs.f2_28NA(), 0.01);
            Assert.AreEqual(-157.4, evs.f2_29(), 0.01);
            Assert.AreEqual(31.36, evs.f2_30NA(), 0.01);
            Assert.AreEqual(5.24, evs.f2_31NB(), 0.01);
            Assert.AreEqual(109.52, evs.f2_32NA(), 0.01);
            Assert.AreEqual(83.4, evs.f2_33NA(), 0.01);
            Assert.AreEqual(79.91, evs.f1_22(), 0.01); //f2_31
            // no f2_32
            Assert.AreEqual(0.26, evs.f1_24(), 0.01); //f2_33
            Assert.AreEqual(57.07, evs.f1_25(), 0.01); //f2_34
            Assert.AreEqual(29.29, evs.f1_26(), 0.01); //f2_35
            Assert.AreEqual(7.59, evs.f2_36(), 0.01);
            Assert.AreEqual(204.45, evs.f1_27(), 0.01); //f2_37
            Assert.AreEqual(212.04, evs.f2_38(), 0.01);
            Assert.AreEqual(0.20, evs.f1_28(), 0.01); //f2_39
            Assert.AreEqual(7.1, evs.f1_29(), 0.01); //f2_40
            Assert.AreEqual(7.17, evs.f1_30(), 0.01); //f2_41
            Assert.AreEqual(211.62, evs.f1_31(), 0.01); //f2_42
            Assert.AreEqual(219.21, evs.f2_43(), 0.01);
            Assert.AreEqual(49.97, evs.f1_32(), 0.01); //f2_44
            Assert.AreEqual(1.81, evs.f1_33(), 0.01); //f2_45
            Assert.AreEqual(27, evs.f2_46NA(), 0.01);
            Assert.AreEqual(58.32, evs.f2_46(), 0.01);
            Assert.AreEqual(223.48, evs.f2_46NB(), 0.01); 
            Assert.AreEqual(208.32, evs.f2_47(), 0.01);
            Assert.AreEqual(248.14, evs.f2_47NA(), 0.01);
            Assert.AreEqual(398.14, evs.f2_48NA(), 0.01);
            Assert.AreEqual(2.81, evs.f2_48NB(), 0.01);
            Assert.AreEqual(9.99, evs.f2_48NC(), 0.01);
            Assert.AreEqual(-124.43, evs.f2_48ND(), 0.01);
            Assert.AreEqual(-127.25, evs.f2_48(), 0.01);
            Assert.AreEqual(-205.41, evs.f2_49(), 0.01);
            Assert.AreEqual(-202.6, evs.f2_49NA(), 0.01);
            Assert.AreEqual(-105.26, evs.f2_52(), 0.01);
            Assert.AreEqual(-112.85, evs.f2_53(), 0.01);
            Assert.AreEqual(-112.79, evs.f2_54(), 0.01);
            Assert.AreEqual(-121.55, evs.f1_40(), 0.01);  // f2_55
            Assert.AreEqual(-129.15, evs.f2_56(), 0.01);
            Assert.AreEqual(33.88, evs.f2_57(), 0.01);
            Assert.AreEqual(-110.18, evs.f2_57NA(), 0.01);
            Assert.AreEqual(-136.3, evs.f2_57NB(), 0.01);
            Assert.AreEqual(31.07, evs.f2_57NC(), 0.01);
            Assert.AreEqual(21.99, evs.f2_58(), 0.01);
            Assert.AreEqual(11.58, evs.f2_59(), 0.01);
            Assert.AreEqual(100.15, evs.f2_60(), 0.01);
            Assert.AreEqual(89.75, evs.f2_61(), 0.01);
            Assert.AreEqual(20.86, evs.f2_61NA(), 0.01);
            Assert.AreEqual(10.45, evs.f2_61NB(), 0.01);
            Assert.AreEqual(-128.45, evs.f2_62(), 0.01);
            Assert.AreEqual(-138.85, evs.f2_63(), 0.01);
            Assert.AreEqual(17.34, evs.f2_64(), 0.01);
            Assert.AreEqual(34.19, evs.f2_64NA(), 0.01);
            Assert.AreEqual(23.77, evs.f2_64NB(), 0.01);
            Assert.AreEqual(112.35, evs.f2_64NC(), 0.01);
            Assert.AreEqual(101.95, evs.f2_64ND(), 0.01);
            Assert.AreEqual(-7.023, evs.f2_65(), 0.01);
            Assert.AreEqual(95.50, evs.f2_66(), 0.01);
            Assert.AreEqual(24.36, evs.f2_67NA(), 0.01);
            Assert.AreEqual(16.21, evs.f2_68NA(), 0.01);
            Assert.AreEqual(-8.15, evs.f2_69NA(), 0.01);
            Assert.AreEqual(22.98, evs.f2_76(), 0.01);
            Assert.AreEqual(20.19, evs.f2_77(), 0.01);
            Assert.AreEqual(7.171, evs.f1_56(), 0.01);  // f2_78
            Assert.AreEqual(14.77, evs.f2_79(), 0.01);
            Assert.AreEqual(1.053e-10, evs.f2_81(), 1e-12);
            Assert.AreEqual(4.82e-6, evs.f2_82(), 1e-8);


            e.IsRaining = true;
            Assert.AreEqual(219.61, sys.Tx.Lsv, 0.01);  // f2_8
            Assert.AreEqual(229.59, sys.Tx.Lmax, 0.01);  // f2_13
            Assert.AreEqual(6.1, sys.Tx.CN, 0.01);  // f2_25
            Assert.AreEqual(84.26, sys.Tx.CN0, 0.01);  // f2_27

            Assert.AreEqual(83.18, sys.CN0_total, 0.01);  // f2_67
        }

        [TestMethod]
        public void Test_KuTV_Band_Functions()
        {
            Evaluations evs = new Evaluations();
            evs.Rb_pr_czs = 35;
            evs.n_pr_czs = 3;

            evs.equivalent_isotropic_radiated_power = 49.29;

            evs.r_orientationCoefficient = 13.8;
            evs.r_operating_frequency = 12700;
            evs.r_antenna_diameter = 0.6;
            evs.r_attenuation_due_to_rain = 6.838;
            evs.r_L_atm_pr_czs = 10 * Math.Log10(1.1);


            Angosat.Environment e = new Angosat.Environment();
            KuTVSystem sys = new KuTVSystem(e);


            Assert.AreEqual(0.2, evs.f1_28(), 0.01);  // f3_8
            Assert.AreEqual(205.7, evs.f1_31(), 0.01);  // f3_11
            Assert.AreEqual(0.97, evs.f3_14(), 0.01);
            Assert.AreEqual(-7.04, evs.f3_15(), 0.01);
            Assert.AreEqual(119.64, evs.f3_16(), 0.01);
            Assert.AreEqual(36.49, evs.f1_25(), 0.01); // f3_4
            Assert.AreEqual(27, evs.f2_46NA(), 0.01);  // f3_17
            Assert.AreEqual(58.32, evs.f2_46(), 0.01); // f3_19
            Assert.AreEqual(185.82, evs.f3_21(), 0.01);
            Assert.AreEqual(219.67, evs.f2_46NB(), 0.01); // f3_18
            Assert.AreEqual(244.45, evs.f2_47NA(), 0.01); // f3_20
            Assert.AreEqual(365.64, evs.f3_22(), 0.01);
            Assert.AreEqual(13.5, evs.f3_25(), 0.01);
            Assert.AreEqual(10.49, evs.f3_26(), 0.01);
            Assert.AreEqual(1.8e7, evs.f1_36NA(), 1e5);  // 3_27NA
            Assert.AreEqual(13.13, evs.f3_27(), 0.01);
            Assert.AreEqual(2.88, evs.f3_28(), 0.01);
            Assert.AreEqual(10.24, sys.Rx.EbN0, 0.01);  // f3_29
            Assert.AreEqual(162.08, evs.f3_31(), 0.01);
            Assert.AreEqual(-83.78, evs.f3_32(), 0.01);
            Assert.AreEqual(-91.03, evs.f3_33(), 0.01);
            Assert.AreEqual(5.61e-4, evs.f3_34(), 1e-6);
            Assert.AreEqual(2.43e-4, evs.f3_35(), 1e-6);
            Assert.AreEqual(54.97, evs.f3_36(), 0.01);
            Assert.AreEqual(47.72, evs.f3_37(), 0.01);
            Assert.AreEqual(1.944, evs.f1_57(), 0.01);  // f3_40

            e.IsRaining = true;
            Assert.AreEqual(-0.01, sys.Rx.EbN0, 0.01);  // f3_30
        }
    }
}
