﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angosat
{
    //[Serializable]
    public class KuReceiver: Receiver
    {
        protected KuReceiver()
            : base() { }
        public KuReceiver(Environment environment, Satellite satellite)
            : base(environment, satellite)
        {
            EIIM = 49.29;
            Latm = 10 * Math.Log10(1.1);
            A001 = 7.18;
            f = 11160;
            AntennaDiameter = 7.3;
            Oc = 1.3;
        }

        public override double fMax
        {
            get { return 11160; }
        }
        public override double fMin
        {
            get { return 10990; }
        }

        // f2_46, f2_47NA
        public override double Ta
        {
            get
            {
                double laft1 = 1.0 / Math.Pow(10, Laft / 10);
                double latm1 = 1.0 / Math.Pow(10, Latm / 10);
                return base.Ta + laft1 * (1 - latm1) * (Tatm - Ts);
            }
        }

        // f2_48NB
        public double B
        {
            get
            {
                bool rain = _environment.IsRaining;

                _environment.IsRaining = false;
                double t = T_total;

                _environment.IsRaining = true;
                double t_rain = T_total;

                _environment.IsRaining = rain;
                return 10 * Math.Log10(t_rain / t);
            }
        }

        // f2_48NC
        public double DND
        {
            get
            {
                return A001 + B;
            }
        }
    }
}
