﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Angosat
{
    [XmlInclude(typeof(CSender))]
    [XmlInclude(typeof(KuSender))]
    [Serializable]
    public abstract class Sender : Transmitter
    {
        protected Sender()
            : base(){}
        public Sender(Environment environment, Satellite satellite)
            : base(environment, satellite)
        {
        }

        protected const double laft = 3;
        protected const double gt = -6;

        protected abstract double Pym { get; }
        protected abstract double YMnas { get; }
        protected abstract double Gsr { get; }
        protected override double Laft
        {
            get
            {
                return laft;
            }
        }

        public override double GT
        {
            get
            {
                return gt;
            }
        }

        // f1_15NA, f2_17NA
        public double OBO
        {
            get
            {
                return 10 * Math.Log10(Pym / YMnas);
            }
        }

        // f1_15, f2_18NA
        public override double P
        {
            get
            {
                return 10 * Math.Log10(Pym) + Gmax - Laft - OBO;
            }
        }

        // f1_16, f2_20
        public override double Psi
        {
            get
            {
                return P + base.Psi;
            }
        }

        // f1_17NA, f2_23NA, f2_24NB
        public override double C
        {
            get
            {
                return P + Gsr - Lsv;
            }
        }

        // f1_18, f2_24, f2_25
        public override double CN
        {
            get
            {
                return P + GT - 10 * Math.Log10(k * delta_f) - Lmax;
            }
        }

        // f1_19, f2_26, f2_27
        public override double CN0
        {
            get
            {
                return CN + 10 * Math.Log10(delta_f);
            }
        }
    }
}
