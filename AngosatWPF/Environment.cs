﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angosat
{
    [Serializable]
    public class Environment
    {
        public static readonly double light_speed = 3e8;  // скорость света
        public bool IsRaining { get; set; }
    }
}
