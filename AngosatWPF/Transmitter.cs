﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angosat
{
    public abstract class Transmitter
    {
        protected readonly Satellite _satellite;
        protected readonly Environment _environment;

        protected Transmitter() {}
        public Transmitter(Environment environment, Satellite satellite)
        {
            _environment = environment;
            _satellite = satellite;

            AntennaLatitude = 8.5;
            AntennaLongitude = 14.5;
            FEC = 7.0 / 8.0;
            n = 2;
            Rb = 85;
            rolloff = 1.35;
        }

        protected const double R = 6378000; // earth radius
        protected const double h = 35786000;
        protected const double K11 = 6.63;
        protected const double ku = 0.7;  // коэффициент использования поверхности антенны
        protected const double Lpol = 0.07;
        protected const double k = 1.38e-23;

        public double AntennaLatitude { get; set; }
        public double AntennaLongitude { get; set; }
        public double AntennaDiameter { get; set; }
        // operating frequency
        public abstract double fMax { get; }
        public abstract double fMin { get; }
        protected double _f;
        public double f {
            get 
            {
                return _f;
            }
            set 
            {
                if (fMax < value || value < fMin)
                {
                    throw new ArgumentOutOfRangeException(String.Format("Значение должно быть в диапазоне {0}-{1}", fMin, fMax));
                }
                _f = value;
            }
        }  
        public double Oc { get; set; }  // orientation coefficient
        public abstract double T_total { get; }
        public double Rb { get; set; }
        public double n { get; set; }
        public double FEC { get; set; }
        public double rolloff { get; set; }
        public double Latm { get; set; }
        
        
        // f1_1
        public double d
        {
            get
            {
                double x1 = AngoMath.DegCos(AntennaLatitude);
                double x2 = AngoMath.DegCos(_satellite.Longitude - AntennaLongitude);
                return Math.Sqrt(h * h + 2.0 * R * (h + R) * (1.0 - x1) * x2) / 1e3;
            }
        }

        // f1_2
        public double Ym
        {
            get
            {
                double a = K11 * AngoMath.DegCos(_satellite.Longitude - AntennaLongitude) * AngoMath.DegCos(AntennaLatitude);
                double x = a - 1;
                double y = Math.Sqrt(K11 * K11 + 1 - 2 * a);
                return AngoMath.toDeg(Math.Asin(x / y));
            }
        }

        // f1_3
        public double CosPsi
        {
            get
            {
                double la1 = AngoMath.DegSin(AntennaLatitude);
                double cx = AngoMath.DegCos(_satellite.Longitude - AntennaLongitude);
                double bx = R / (R + h);
                double x = la1 * (1.0 - bx * cx);
                double y = Math.Sqrt(1.0 - cx * cx);
                double la2 = AngoMath.DegCos(AntennaLatitude);
                double z = Math.Sqrt(1.0 - 2.0 * bx * cx + bx * bx * la2 * la2);
                double r = x / (y * z);
                return AngoMath.toDeg(Math.Cos(x / (y * z)));
            }
        }

        // f1_4
        public double ThetaZdB
        {
            get
            {
                return 70.0 * Environment.light_speed / (f * AntennaDiameter * 1e6);
            }
        }

        // f1_5
        public double Gmax
        {
            get
            {
                return 9.94 +
                       10 * Math.Log10(ku) +
                       20 * Math.Log10(f * 1e6 * AntennaDiameter / Environment.light_speed);
            }
        }

        // f1_6, 2_8, 2_37, 2_38
        public double Lsv
        {
            get
            {
                double lsv = 32.4 + 20 * (Math.Log10(f) + Math.Log10(d));
                if (_environment.IsRaining)
                    lsv += LA;
                return lsv;
            }
        }

        // f1_7
        public double Theta
        {
            get
            {
                return ThetaZdB / Oc;
            }
        }

        // f1_8
        public double Lop
        {
            get
            {
                return 12.0 * Math.Pow(Theta / ThetaZdB, 2);
            }
        }

        // f1_9
        public double Ldop
        {
            get
            {
                return Lpol + Lop;
            }
        }

        // f1_10, f2_13, f2_43
        public double Lmax
        {
            get
            {
                return Ldop + Lsv;
            }
        }

        // f1_11, f2_44
        public virtual double G
        {
            get
            {
                return Gmax - Lop;
            }
        }

        // R_c_per_czs
        public double Rc
        {
            get
            {
                return Rb * 1e6 / (FEC * n);
            }
        }

        // f1_13NA
        public double delta_f
        {
            get
            {
                return Rc * rolloff;
            }
        }

        // f1_13, f2_16, f2_48, f2_48ND
        public double N
        {
            get
            {
                return 10 * Math.Log10(k * T_total * delta_f);
            }
        }

        // f1_14, f2_17, f2_49, f2_49NA
        public double N0
        {
            get
            {
                return 10 * Math.Log10(k * T_total);
            }
        }

        protected abstract double Laft { get; }
        public abstract double P { get; }
        // f1_16 part
        public virtual double Psi
        {
            get
            {
                return -20 * Math.Log10(d * 1e3) - 10 * Math.Log10(4 * Math.PI);
            }
        }
        public abstract double GT { get; }

        // f1_17, f2_22, f2_56
        public double E
        {
            get
            {
                return P - 20 * Math.Log10(d) + 74.8;
            }
        }

        // f1_17NA
        public abstract double C { get; }
        public abstract double CN { get; }
        public abstract double CN0 { get; }

        // f1_20, f2_28, f2_29, f2_62, f2_63
        public double CT
        {
            get
            {
                return CN0 + 10 * Math.Log10(k);
            }
        }

        // f1_20NA, f2_27NA, f2_28NA, f2_61NA, f2_61NB
        public virtual double EbN0
        {
            get
            {
                return CN0 - 10 * Math.Log10(Rb * 1e6);
            }
        }

        // f1_21
        public double Gamma
        {
            get
            {
                return Rb * 1e6 / delta_f;
            }
        }










        // Ku
        public double A001 { get; set; }  // attenuation_due_to_rain
        
        // f2_6, f2_36
        public double LA
        {
            get
            {
                return A001 + Latm;
            }
        }

        // f2_30NA, f2_31NB, f2_64NA, f2_64NB
        public double CICN
        {
            get
            {
                return CN + 12.2;
            }
        }

        // f2_32NA, f2_33NA, f2_64NC, f2_64ND
        public double CICN0
        {
            get
            {
                return CN0 + 12.2;
            }
        }
    }
}
