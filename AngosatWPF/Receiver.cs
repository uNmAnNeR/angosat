﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Angosat
{
    [XmlInclude(typeof(CReceiver))]
    [XmlInclude(typeof(KuReceiver))]
    [XmlInclude(typeof(KuTVReceiver))]
    [Serializable]
    public abstract class Receiver : Transmitter
    {
        protected Receiver()
            :base() { }
        public Receiver(Environment environment, Satellite satellite)
            : base(environment, satellite)
        {
            Te = 150;
            EIIM = 39.86;
            Ysun = 72.71;
        }


        public double Te { get; set; }
        protected const double T0 = 290;
        protected const double Tatm = 270;  // температура окружающей атмосферы
        protected const double Tg = 2.7;
        protected virtual double Ts { get { return 15; } }
        protected override double Laft { get { return 0.15; } }

        public double EIIM { get; set; }  // ЭИИМ.ср
        public double Ysun { get; set; }  // r_angle_between_sun_and_ZS
        

        // f1_26
        public double S
        {
            get
            {
                return Math.Pow(10, 0.1 * Gmax) * Environment.light_speed * Environment.light_speed /
                       (4 * Math.PI * Math.Pow(f * 1e6, 2));
            }
        }
        
        // f1_33, f2_45
        public double F
        {
            get
            {
                return 10 * Math.Log10(1 + Te / T0);
            }
        }

        // f1_33NA, f2_46NA, f2_46NB
        public double Ta0
        {
            get
            {
                double latm = Latm;
                if (_environment.IsRaining)
                    latm += A001;
                double a1 = Math.Pow(10, -0.1 * latm);  // temp var
                return (1 - a1) * Tatm + a1 * Tg;
            }
        }

        // f1_34
        public virtual double Ta
        {
            get
            {
                double a = 1.0 / Math.Pow(10, Laft / 10);
                return Ta0 * a + (1 - a) * T0;
            }
        }

        // f1_35, f2_47, f2_48NA
        public override double T_total
        {
            get
            {
                return Ta + T0 * (Math.Pow(10, 0.1 * F) - 1);
            }
        }

        // f1_38, f2_52
        public override double P
        {
            get
            {
                return C - Ldop;
            }
        }

        // f1_39, f2_54
        public override double Psi
        {
            get
            {
                return EIIM + base.Psi;
            }
        }

        // f1_41NA, f2_57NA, f2_57NB
        public override double C
        {
            get
            {
                return EIIM + Gmax - Lsv;
            }
        }

        // f1_41, f2_57, f2_57NC
        public override double GT
        {
            get
            {
                return Gmax - 10 * Math.Log10(T_total);
            }
        }

        // f1_42, f2_58, f2_59, f3_27, f3_28
        public override double CN
        {
            get
            {
                return EIIM + GT - 10 * Math.Log10(k * delta_f) - Lmax;
            }
        }

        // f1_43, f2_60, f2_61
        public override double CN0
        {
            get
            {
                return EIIM + GT - 10 * Math.Log10(k) - Lmax;
            }
        }

        // f1_47
        public double Tsun
        {
            get
            {
                return 0.5 * 120000 / Math.Pow(f * 1e-3, 0.75);
            }
        }

        // f1_48, f2_69
        protected double Atten
        {
            get
            {
                return 0.036 / AngoMath.DegSin(Ysun);
            }
        }

        // f1_49, f2_70
        public double T2sun
        {
            get
            {
                return Tsun * Math.Pow(10, -0.1 * Atten);
            }
        }

        // f1_50, f2_71
        public double delta_T
        {
            get
            {
                return T2sun * Math.Pow(0.53 / ThetaZdB, 2);
            }
        }

        // f1_51, f2_72
        public double delta_Tn
        {
            get
            {
                return 0.5 * T2sun;
            }
        }

        // f1_52, f2_73
        public double AD
        {
            get
            {
                return (ThetaZdB + 0.48) / 0.4;
            }
        }

        // f1_53, f2_74
        public double AM
        {
            get
            {
                return (ThetaZdB + 0.48) / 0.25;
            }
        }

        // f1_54, f2_75
        public double TD
        {
            get
            {
                return Math.PI * Math.Pow(ThetaZdB + 0.48, 2) / (0.25 * 4 * 0.4);
            }
        }

        // f1_55, f2_76, f2_77
        public double delta_CN
        {
            get
            {
                return 10 * Math.Log10(1 + delta_T / T_total);
            }
        }

        // f1_56, f2_78, f2_79
        public double M
        {
            get
            {
                return EIIM + GT - EbN0 - 10 * Math.Log10(Rb * 1e6) - 10 * Math.Log10(k) - Lsv;
            }
        }

        // f1_58, f2_81, f2_82
        public double Pv
        {
            get
            {
                return AngoMath.erfc(Math.Sqrt(EbN0));
            }
        }
    }
}
