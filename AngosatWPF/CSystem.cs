﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angosat
{
    [Serializable]
    public class CSystem: NetSystem
    {
        public CSystem(Environment e)
            : base(e) 
        {
            _Tx = new CSender(e, _S);
            _Rx = new CReceiver(e, _S);
        }
    }
}
