﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Angosat;


namespace AngosatWPF
{
    /// <summary>
    /// Interaction logic for SenderInput.xaml
    /// </summary>
    public partial class ParametersInput : Window
    {
        private readonly Case _case;
        public Case Case { get { return _case; } }
        public ParametersInput(Case c)
        {
            _case = c;
            InitializeComponent();
            DataContext = this;

            if (_case.Type == Case.SystemType.C)
            {
                var kuOnlyParameters = (from FrameworkElement co in Parameters.Children
                                        where "KuOnly".Equals(co.Tag)
                                        select co);
                foreach (var co in kuOnlyParameters.ToArray())  // ToArray makes temp copy!
                {
                    Parameters.Children.Remove(co);
                }
            }
            else if (_case.Type == Case.SystemType.KuTV)
            {
                // remove all sender fields (KuTV is only about receiver)
                foreach (DockPanel dp in Parameters.Children)
                {
                    dp.Children.RemoveAt(dp.Children.Count - 1);
                }
            }
        }

        private bool ValidateText(TextBox tb, string text)
        {
            double d;
            text = text.Replace(',', '.');
            text = text.TrimEnd('.');
                //text = text;
            return Double.TryParse(text, out d);
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !ValidateText(e.Source as TextBox, e.Text);
        }
    }
}
