﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angosat
{
    //[Serializable]
    public class CReceiver: Receiver
    {
        protected CReceiver()
            : base() { }
        public CReceiver(Environment environment, Satellite satellite)
            : base(environment, satellite)
        {
            EIIM = 39.86;
            AntennaDiameter = 9;
            Latm = 10 * Math.Log10(1.33);
            Oc = 2.8;
            f = 4130;
        }

        public override double fMax
        {
            get { return 4130; }
        }
        public override double fMin
        {
            get { return 3570; }
        }
    }
}
