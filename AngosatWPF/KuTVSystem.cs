﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angosat
{
    [Serializable]
    public class KuTVSystem: NetSystem
    {
        public KuTVSystem(Environment e)
            : base(e) 
        {
            _Rx = new KuTVReceiver(e, _S);
        }
    }
}
