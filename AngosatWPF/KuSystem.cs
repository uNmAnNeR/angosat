﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angosat
{
    [Serializable]
    public class KuSystem: NetSystem
    {
        public KuSystem(Environment e)
            : base(e) 
        {
            _Tx = new KuSender(e, _S);
            _Rx = new KuReceiver(e, _S);
        }
    }
}
