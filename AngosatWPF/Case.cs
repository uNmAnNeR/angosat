﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Angosat
{
    [Serializable]
    public partial class Case
    {
        private NetSystem _system;
        private Environment _environment;
        private SystemType _type;

        public enum SystemType { C, Ku, KuTV };

        public NetSystem System { get { return _system; } }
        public Environment Environment { get { return _environment; } }
        public SystemType Type { get { return _type; } }

        private void MakeSystem() {
            switch (_type)
            {
                case SystemType.C:
                    _system = new CSystem(_environment);
                    break;
                case SystemType.Ku:
                    _system = new KuSystem(_environment);
                    break;
                case SystemType.KuTV:
                    _system = new KuTVSystem(_environment);
                    break;
            }
        }

        public Case(SystemType t)
        {
            _environment = new Environment();
            _type = t;
            MakeSystem();
        }
    }
}
