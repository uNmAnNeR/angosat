﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angosat
{
    //[Serializable]
    public class CSender : Sender
    {
        protected CSender()
            : base(){}
        public CSender(Environment environment, Satellite satellite)
            : base(environment, satellite)
        {
            Oc = 1.8;
            AntennaDiameter = 9;
            f = 6355;
        }

        public override double T_total { get { return 501.18; } }
        protected override double Gsr { get { return 22; } }
        protected override double Pym { get { return 1000; } }
        protected override double YMnas { get { return 1100; } }
        public override double fMax
        {
            get { return 6355; }
        }
        public override double fMin
        {
            get { return 5795; }
        }
    }
}
