﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angosat
{
    public class SystemCalculationsSummary
    {
        private NetSystem _system;
        public SystemCalculationsSummary(NetSystem system)
        {
            _system = system;
        }

        private Dictionary<string, string> _transmitterProperties = new Dictionary<string, string>()
        {
            {"d", "Расстояние ЦЗС - СР, СР - ЦЗС, км"},
            {"Ym", "Угол между ЗС и спутником, Град"},
            {"CosPsi", "Угол поляризации ЦЗС в плоскости перпендикулряной к плоскости экватора, Град"},
            {"ThetaZdB", "Ширина диаграммы направленности антенны, Град"},
            {"Gmax", "Максимальный коэффциент усиления, дБ"},
            {"Lsv", "Затухание сигнала в свободном пространстве, дБ"},
            {"Theta", "Угол неточности наведения антенны, дБ"},
            {"Lop", "Потери из-за неточности наведения антенны, дБ"},
            {"Ldop", "Суммарные дополнительные потери, дБ"},
            {"Lmax", "Максимальные потери, дБ"},
            {"G", "Коэффициент усиления антенны, дБ"},
            {"T_total", "Суммарная шумовая температура приемного тракта СР, К"},
            {"N", "Максимальная мощность теплового шума, Вт"},
            {"N0", "Спектральная плотность мощности шума, Вт/Гц"},
            {"P", "Эффективная мощность на ЗС, дБ·Вт"},
            {"Psi", "Плотность потока мощности насыщения, дБ·Вт/м²"},
            {"E", "Напряженность электромагнитного поля Е, дБ·Вт"},
            {"C", "Мощность на несущей, дБ"},
            {"CN", "Отношение сигнал-шум, дБ·Вт"},
            {"CN0", "Отношение сигнал-шум относительно спектральной плотности мощности, дБ·Вт"},
            {"Rc", "Скорость передачи символов"},
            {"delta_f", "Полоса пропускания ЛСС"},
            {"EbN0", "Отношение энергии бит к спектральной мощности шума, дБ"},
            {"CT", "Отношение несущей и суммарной температуры, дБ·Гц"},
            {"Gamma", "Спектральная эффективность"},

            // Ku
            {"LA", "Суммарный атмосферный вклад в виде затухания, дБ"},
            {"CICN", "Допустимый критерий C/I"},
            {"CICN0", "Допустимый критерий C/I при передаче циврового сигнала"},

            // KuTV
            {"A", "Эффективная площадь покрытия антенны приемника, м²"},
            {"Sigma", "Проницаемость среды"},
            {"Ls", "Расходимость луча, дБ"},
            {"UV", "Уровень выходного сигнал LNB, дБмкВ"}
        };

        private Dictionary<string, string> _receiverProperties = new Dictionary<string, string>()
        {
            {"S", "Эффективная площадь покрытия ЗС, м²"},
            {"F", "Общий шум-фактор премника, дБ"},
            {"Ta0", "Шумовая температура при ясном небе, К"},
            {"Ta", "Эквивалентная шумовая температура антенны, К"},
            {"GT", "Добротность, дБ/К"},
            {"Tsun", "Радиояркостная температура в С диапазоне, К"},
            {"T2sun", "Солнечная шумовая температура на ЗС, К"},
            {"delta_T", "Увеличение солнечной шумовой температуры, К"},
            {"delta_Tn", "Увеличение солнечной шумовой температуры при использование поляризатором, К"},
            {"AD", "Количество дней влияния Солнца на ЗС, день"},
            {"AM", "Максимальное время влияния Солнца на ЗС, мин"},
            {"TD", "Общая продолжительность времени влияния Солнца на ЗС, мин"},
            {"delta_CN", "Ухудшение отношения сигнал-шум из-за Солнечной температуры, дБ"},
            {"M", "Энергетический запас линии связи"},
            {"Pv", "Вероятность битовой ошибки"},

            // Ku
            {"B", "Возрастание шумов"},
            {"DND", "Снижение эффективности ЛСС"}
        };

        private Dictionary<string, string> _senderProperties = new Dictionary<string, string>()
        {
            {"OBO", "Потери выходной мощности на одной несущей, дБ"}
        };

        private Dictionary<string, string> _systemProperties = new Dictionary<string, string>()
        {
            {"CN_total", "Суммарное отношение сигнал-шум при мощности теплового шума, дБ"},
            {"CN0_total", "Суммарное отношение сигнал-шум при спектральной плотности мощности шума, дБ·Гц"},
            {"EbN0_total", "Суммарное отношение энергия бита к спектральная плотность, дБ"},

            // Ku
            {"M_total_CN", "Суммарное энергетическое запас на все линии"}
        };

        private double propertyValue(object o, string propName)
        {
            try
            {
                var p = o.GetType().GetProperty(propName);
                return (p != null && p.CanRead) ? (double)p.GetValue(o) : Double.NaN;
            }
            catch  // sorry for that, no time..
            {
                return Double.NaN;
            }
        }

        public List<Tuple<string, double, double>> TransmitterProperties
        {
            get
            {
                var props = new List<Tuple<string, double, double>>();
                foreach (var pair in _transmitterProperties)
                {
                    string propertyName = pair.Key;
                    string propertyLabel = pair.Value;
                    double txPValue = propertyValue(_system.Tx, propertyName);
                    double rxPValue = propertyValue(_system.Rx, propertyName);
                    if (!Double.IsNaN(txPValue) || !Double.IsNaN(rxPValue))
                        props.Add(Tuple.Create(propertyLabel, txPValue, rxPValue));
                }
                return props;
            }
        }

        private List<Tuple<string, double>> objectProperties(object o, Dictionary<string, string> props)
        {
            List<Tuple<string, double>> calcs = new List<Tuple<string, double>>();
            foreach (var pair in props)
            {
                string pName = pair.Key;
                string pLabel = pair.Value;
                double pValue = propertyValue(o, pName);
                if (!Double.IsNaN(pValue))
                    calcs.Add(Tuple.Create(pLabel, pValue));
            }
            return calcs;
        }

        public List<Tuple<string, double>> ReceiverProperties
        {
            get
            {
                return objectProperties(_system.Rx, _receiverProperties);
            }
        }

        public List<Tuple<string, double>> SenderProperties
        {
            get
            {
                return objectProperties(_system.Tx, _senderProperties);
            }
        }

        public List<Tuple<string, double>> SystemProperties
        {
            get
            {
                return objectProperties(_system, _systemProperties);
            }
        }

        private string FormatDouble(double d)
        {
            if (Double.IsNaN(d))
                return "";
            return d.ToString("G3");
        }

        public List<Tuple<string, string, string>> PrintableTransmitterProperties
        {
            get
            {
                var pprops = new List<Tuple<string, string, string>>();
                foreach (var p in TransmitterProperties)
                {
                    pprops.Add(Tuple.Create(p.Item1, FormatDouble(p.Item2), FormatDouble(p.Item3)));
                }

                foreach (var p in SenderProperties)
                {
                    pprops.Add(Tuple.Create(p.Item1, FormatDouble(p.Item2), ""));
                }

                foreach (var p in ReceiverProperties)
                {
                    pprops.Add(Tuple.Create(p.Item1, "", FormatDouble(p.Item2)));
                }

                return pprops;
            }
        }

        public List<Tuple<string, string>> PrintableSystemProperties
        {
            get
            {
                var pprops = new List<Tuple<string, string>>();
                foreach (var p in SystemProperties)
                {
                    pprops.Add(Tuple.Create(p.Item1, FormatDouble(p.Item2)));
                }
                return pprops;
            }
        }
    }
}
