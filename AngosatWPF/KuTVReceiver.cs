﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angosat
{
    //[Serializable]
    public class KuTVReceiver: KuReceiver
    {
        protected KuTVReceiver()
            : base() { }
        public KuTVReceiver(Environment environment, Satellite satellite)
            : base(environment, satellite)
        {
            EIIM = 49.29;
            Latm = 10 * Math.Log10(1.1);
            A001 = 6.838;
            Oc = 13.8;
            AntennaDiameter = 0.6;
            f = 12700;
            Rb = 35;
        }

        private double Fsh = 1.5;
        private double alpha = 0.3;
        private double R1 = 75;

        public override double fMax { get { return 12700; } }
        public override double fMin { get { return 10700; } }

        protected override double Ts { get { return 290; } }

        // f3_14
        public double Sigma
        {
            get
            {
                return Math.Pow(10, -0.1 * Laft);
            }
        }

        // f3_15
        public double A
        {
            get
            {
                double d = 0.5 * AntennaDiameter;
                return 10 * Math.Log10(Math.PI * d * d * ku);
            }
        }

        // f3_16
        public double Tsh
        {
            get
            {
                return 290 * (Math.Pow(10, 0.1 * Fsh) - 1);
            }
        }

        // f3_21, f3_22
        public override double T_total
        {
            get
            {
                return Tsh + (1 - Sigma) * Ts + Sigma * Ta;
            }
        }

        // f3_25, f3_26
        public override double GT
        {
            get
            {
                double a = Gmax - alpha;
                if (_environment.IsRaining)
                    a -= Lop;
                return 10 * Math.Log10(Math.Pow(10, 0.1 * a) / T_total);
            }
        }

        // f3_29, f3_30
        public override double EbN0
        {
            get
            {
                return CN + 10 * (Math.Log10(1.0 / (Rb * 1e6)) + Math.Log10(delta_f));
            }
        }

        // f3_31
        public double Ls
        {
            get
            {
                double D = d * 1e3;
                return 10 * Math.Log10(4 * Math.PI * D * D);
            }
        }

        // f3_32, f3_33
        public override double C
        {
            get
            {
                double c = base.C + A - alpha - Ldop;
                if (_environment.IsRaining)
                    c -= LA;
                return c;
            }
        }

        // f3_34, f3_35
        public double V
        {
            get
            {
                return Math.Sqrt(Math.Pow(10, 0.1 * C) * R1);
            }
        }

        // f3_36, f3_37
        public double UV
        {
            get
            {
                return 20 * Math.Log10(V * 1e6);
            }
        }
    }
}
