﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Angosat
{
    public partial class Case : IXmlSerializable
    {
        public System.Xml.Schema.XmlSchema GetSchema() { return null; }
        protected Case()
        {
            _environment = new Environment();
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            reader.MoveToContent();
            _type = (SystemType)Enum.Parse(typeof(SystemType), reader.GetAttribute("Type"));

            reader.ReadStartElement();
            // Environment
            if (!reader.IsEmptyElement)
            {
                XmlSerializer s = new XmlSerializer(typeof(Environment));
                _environment = (Environment)s.Deserialize(reader);
            }


            MakeSystem();


            // Satelite
            if (!reader.IsEmptyElement)
            {
                XmlSerializer s = new XmlSerializer(typeof(Satellite));
                Satellite sat = (Satellite)s.Deserialize(reader);
                CopyProperties(sat, System.Satellite);
            }


            // Sender
            if (!reader.IsEmptyElement)
            {
                XmlSerializer s = new XmlSerializer(typeof(Sender));
                Sender sender = (Sender)s.Deserialize(reader);
                CopyProperties(sender, System.Tx);
            }

            // Receiver
            if (!reader.IsEmptyElement)
            {
                XmlSerializer s = new XmlSerializer(typeof(Receiver));
                Receiver r = (Receiver)s.Deserialize(reader);
                CopyProperties(r, System.Rx);
            }
        }

        private static void CopyProperties(object src, object dst)
        {
            foreach (System.Reflection.PropertyInfo pi in src.GetType().GetProperties())
            {
                if (pi.CanWrite)
                {
                    pi.SetValue(dst, pi.GetValue(src, null), null);
                }
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("Type", _type.ToString());

            XmlSerializer s = new XmlSerializer(typeof(Environment));
            s.Serialize(writer, Environment);

            s = new XmlSerializer(typeof(Satellite));
            s.Serialize(writer, System.Satellite);

            s = new XmlSerializer(typeof(Sender));
            s.Serialize(writer, System.Tx);

            s = new XmlSerializer(typeof(Receiver));
            s.Serialize(writer, System.Rx);
        }
    }
}
