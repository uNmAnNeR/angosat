﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Angosat;

namespace AngosatWPF
{
    /// <summary>
    /// Interaction logic for SystemTypeChoice.xaml
    /// </summary>
    public partial class SystemTypeChoice : Window
    {
        public enum ActionType { Open, New }

        private Case.SystemType _type;
        public Case.SystemType ChosenType { get { return _type; } }

        private ActionType _action;
        public ActionType Action { get { return _action; } }

        public SystemTypeChoice()
        {
            InitializeComponent();
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            if (Ctype.IsChecked == true)
                _type = Case.SystemType.C;
            else if (Kutype.IsChecked == true)
                _type = Case.SystemType.Ku;
            else
                _type = Case.SystemType.KuTV;
            _action = ActionType.New;
            this.DialogResult = true;
            this.Close();
        }

        private void openButton_Click(object sender, RoutedEventArgs e)
        {
            _action = ActionType.Open;
            this.DialogResult = true;
            this.Close();

        }
    }
}
