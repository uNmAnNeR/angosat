﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angosat
{
    [Serializable]
    public abstract class NetSystem
    {
        protected readonly Environment _environment;
        protected Satellite _S;
        protected Sender _Tx;
        protected Receiver _Rx;

        public Satellite Satellite { get { return _S; } }
        public Sender Tx { get { return _Tx; } }
        public Receiver Rx { get { return _Rx; } }

        public NetSystem(Environment e)
        {
            _environment = e;
            _S = new Satellite();
        }

        private double _cn_total(double txCN, double rxCN)
        {
            return 10 * Math.Log10(1 / (1 / Math.Pow(10, txCN * 0.1) +
                                        1 / Math.Pow(10, rxCN * 0.1)));
        }

        // f1_45, f2_64, f2_65
        public double CN_total
        {
            get
            {
                return _cn_total(_Tx.CN, _Rx.CN);
            }
        }

        // f1_46, f2_66, f2_67
        public double CN0_total
        {
            get
            {
                return _cn_total(_Tx.CN0, _Rx.CN0);
            }
        }

        // f1_46NA, f2_68NA, f2_69NA
        public double EbN0_total
        {
            get
            {
                return -10 * Math.Log10(Math.Pow(10, -0.1 * _Tx.EbN0) + 
                                        Math.Pow(10, -0.1 * _Rx.EbN0));
            }
        }

        // f2_67NA
        public double M_total_CN
        {
            get
            {
                bool rain = _environment.IsRaining;
                
                _environment.IsRaining = false;
                double cn = CN_total;
                
                _environment.IsRaining = true;
                double cn_rain = CN_total;

                _environment.IsRaining = rain;
                return cn - cn_rain;
            }
        }

        // f2_67NA2
        public double M_total_CN0
        {
            get
            {
                bool rain = _environment.IsRaining;

                _environment.IsRaining = false;
                double cn0 = CN0_total;

                _environment.IsRaining = true;
                double cn0_rain = CN0_total;

                _environment.IsRaining = rain;
                return cn0 - cn0_rain;
            }
        }
    }
}
