﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Angosat
{
    public class CalculationsPaginator: DocumentPaginator
    {
        private FrameworkElement Element;

        public CalculationsPaginator(FrameworkElement element) { Element = element; }

        public override DocumentPage GetPage(int pageNumber)
        {

            Element.RenderTransform =
                new TranslateTransform(-PageSize.Width * (pageNumber % Columns), -PageSize.Height * (pageNumber / Columns));

            Size elementSize = new Size(Element.ActualWidth, Element.ActualHeight);
            Element.Measure(elementSize);
            Element.Arrange(new Rect(new Point(_margin.Width, _margin.Height), elementSize));

            var page = new DocumentPage(Element);
            Element.RenderTransform = null;

            return page;
        }

        public override bool IsPageCountValid { get { return true; } }

        public int Columns
        {
            get
            {
                return (int)Math.Ceiling(Element.ActualWidth / PageSize.Width);
            }
        }

        public int Rows
        {
            get { return (int)Math.Ceiling(Element.ActualHeight / PageSize.Height); }
        }

        public override int PageCount
        {
            get { return Columns * Rows; }
        }

        private Size _margin = new Size(20, 200);
        private Size _pageSize;
        public override Size PageSize
        {
            set
            {
                _pageSize = new Size(value.Width - _margin.Width * 2,
                                     value.Height - _margin.Height * 2);
            }

            get
            {
                return _pageSize;
            }
        }

        public override IDocumentPaginatorSource Source
        {
            get { return null; }
        }
    }
}
