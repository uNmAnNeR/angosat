﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Angosat;

namespace AngosatWPF
{
    /// <summary>
    /// Interaction logic for CaseControl.xaml
    /// </summary>
    public partial class CaseControl : UserControl
    {
        public Case Case { get; set; }

        public CaseControl(Case c)
        {
            InitializeComponent();
            Case = c;
        }

        private void ChangeInput_Click(object sender, RoutedEventArgs e)
        {
            ParametersInput inputWindow = new ParametersInput(Case);
            Window parentWindow = Window.GetWindow(this);
            inputWindow.Owner = parentWindow;
            inputWindow.ShowDialog();
        }

        private void Calculations_Click(object sender, RoutedEventArgs e)
        {
            Calculations calcWindow = new Calculations(Case);
            Window parentWindow = Window.GetWindow(this);
            calcWindow.Owner = parentWindow;
            calcWindow.ShowDialog();
        }


    }
}
