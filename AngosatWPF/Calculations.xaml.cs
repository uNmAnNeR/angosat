﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Angosat;

namespace AngosatWPF
{
    /// <summary>
    /// Interaction logic for Calculations.xaml
    /// </summary>
    public partial class Calculations : Window
    {
        private Thickness _rowThickness = new Thickness(1);
        private void InsertTransmitterRow(string name, string txValue, string rxValue)
        {
            DockPanel dp = new DockPanel();

            Label nameLabel = new Label();
            nameLabel.Content = name;
            nameLabel.Padding = _rowThickness;

            Label txValueLabel = new Label();
            txValueLabel.Content = txValue;
            txValueLabel.Padding = _rowThickness;
            txValueLabel.Style = FindResource("PushRight") as Style;

            Label rxValueLabel = new Label();
            rxValueLabel.Content = rxValue;
            rxValueLabel.Padding = _rowThickness;
            rxValueLabel.Style = FindResource("PushRight") as Style;

            dp.Children.Add(nameLabel);
            dp.Children.Add(rxValueLabel);
            dp.Children.Add(txValueLabel);

            TransmitterCalculations.Children.Add(dp);
        }

        private void InsertSystemRow(string name, string value)
        {
            DockPanel dp = new DockPanel();

            Label nameLabel = new Label();
            nameLabel.Content = name;
            nameLabel.Padding = _rowThickness;

            Label valueLabel = new Label();
            valueLabel.Content = value;
            valueLabel.Padding = _rowThickness;
            valueLabel.Style = FindResource("PushRight") as Style;

            dp.Children.Add(nameLabel);
            dp.Children.Add(valueLabel);

            SystemCalculations.Children.Add(dp);
        }

        public Calculations(Case c)
        {
            InitializeComponent();
            SystemCalculationsSummary calcs = new SystemCalculationsSummary(c.System);

            foreach (var p in calcs.PrintableTransmitterProperties)
                InsertTransmitterRow(p.Item1, p.Item2, p.Item3);

            var sysProps = calcs.PrintableSystemProperties;

            if (sysProps.Count == 0)
            {
                var parent = VisualTreeHelper.GetParent(SystemCalculationsContainer) as Panel;
                parent.Children.Remove(SystemCalculationsContainer);
            }
            else
            {
                foreach (var p in sysProps)
                    InsertSystemRow(p.Item1, p.Item2);
            }
            ProjectInfo.Content = "Проект (" + (App.Current as App).CurrentCase.Type.ToString() + "): " + (App.Current as App).CurrentCaseFileName;
        }

        private void PrintResults_Click(object sender, RoutedEventArgs e)
        {
            (App.Current as App).printCase();
        }
    }
}
