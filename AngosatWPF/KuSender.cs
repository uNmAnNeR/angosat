﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angosat
{
    //[Serializable]
    public class KuSender : Sender
    {
        protected KuSender() :base() { }
        public KuSender(Environment environment, Satellite satellite)
            : base(environment, satellite)
        {
            Latm = 0.13;
            A001 = 12.93;
            Oc = 1.1;
            f = 14210;
            AntennaDiameter = 7.3;
        }

        public override double fMax { get { return 14210; } }
        public override double fMin { get { return 14040; } }

        public override double T_total { get { return 478.63; } }
        protected override double Gsr { get { return 27; } }
        protected override double Pym { get { return 400; } }
        protected override double YMnas { get { return 500; } }
        public override double GT { get { return 2.1; } }

        // f2_23NA
        public override double C
        {
            get
            {
                return P + Gsr - Lmax;
            }
        }
    }
}
