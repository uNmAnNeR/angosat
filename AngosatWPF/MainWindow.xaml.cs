﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Angosat;

namespace AngosatWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            (App.Current as App).RaiseChangeCaseEvent += MainWindow_RaiseChangeCaseEvent;
            (App.Current as App).RaiseAddCaseEvent += MainWindow_RaiseAddCaseEvent;
            (App.Current as App).RaiseBeforeRemoveCaseEvent += MainWindow_RaiseBeforeRemoveCaseEvent;
        }

        private void MainWindow_RaiseBeforeRemoveCaseEvent(object sender, CaseEventArgs e)
        {
            CaseTabs.Items.RemoveAt((App.Current as App).CurrentCaseIndex);
        }

        void MainWindow_RaiseChangeCaseEvent(object sender, CaseEventArgs e)
        {
            Dispatcher.BeginInvoke((Action)(() => CaseTabs.SelectedIndex = (App.Current as App).CurrentCaseIndex));
        }

        void MainWindow_RaiseAddCaseEvent(object sender, CaseEventArgs e)
        {
            TabItem newCaseItem = new TabItem();
            newCaseItem.Header = e.Case.Type.ToString();
            newCaseItem.Content = new CaseControl(e.Case);
            CaseTabs.Items.Add(newCaseItem);
        }

        private void addCase()
        {
            // Instantiate the dialog box
            SystemTypeChoice dlg = new SystemTypeChoice();

            // Configure the dialog box
            dlg.Owner = this;

            // Open the dialog box modally 
            bool? tt = dlg.ShowDialog();
            if (tt == true)
            {
                switch (dlg.Action)
                {
                    case SystemTypeChoice.ActionType.New:
                        (App.Current as App).addCase(new Case(dlg.ChosenType));
                        break;
                    case SystemTypeChoice.ActionType.Open:
                        (App.Current as App).openCase();
                        break;
                }
            }
        }

        private void removeCase()
        {
            (App.Current as App).removeCase();
        }

        private void changeCase()
        {
            if (CaseTabs.SelectedIndex > 0)
                (App.Current as App).CurrentCaseIndex = CaseTabs.SelectedIndex;
        }
 

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            addCase();
        }

        private void CaseTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.IsLoaded)
            {
                e.Handled = true;
                changeCase();
            }
        }

        private void MenuItem_New(object sender, RoutedEventArgs e)
        {
            addCase();
        }

        private void MenuItem_Save(object sender, RoutedEventArgs e)
        {
            (App.Current as App).saveCase();
        }


        private void MenuItem_SaveAs(object sender, RoutedEventArgs e)
        {
            (App.Current as App).saveCaseAs();
        }

        private void MenuItem_Print(object sender, RoutedEventArgs e)
        {
            (App.Current as App).printCase();
        }

        private void MenuItem_Open(object sender, RoutedEventArgs e)
        {
            (App.Current as App).openCase();
        }

        private void MenuItem_Close(object sender, RoutedEventArgs e)
        {
            (App.Current as App).removeCase();
        }
    }
}
