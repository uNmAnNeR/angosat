﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
//using System.Drawing;
using System.Drawing.Printing;
using Angosat;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace AngosatWPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    /// 
    public class CaseEventArgs : EventArgs
    {
        public CaseEventArgs(Case c) { _c = c; }
        private Case _c;

        public Case Case { get { return _c; } }
    }


    public partial class App : Application
    {
        public event EventHandler<CaseEventArgs> RaiseAddCaseEvent;
        public event EventHandler<CaseEventArgs> RaiseBeforeRemoveCaseEvent;
        public event EventHandler<CaseEventArgs> RaiseChangeCaseEvent;

        private List<Case> _cases = new List<Case>();
        private Dictionary<Case, string> _caseFiles = new Dictionary<Case, string>();
        private Case _currentCase;

        public Case CurrentCase
        {
            get 
            { 
                return _currentCase; 
            }
            set
            {
                _currentCase = value;
                RaiseChangeCaseEvent(this, new CaseEventArgs(value));
            }
        }
        public int CurrentCaseIndex
        { 
            get 
            { 
                return _cases.IndexOf(CurrentCase); 
            }
            set
            {
                CurrentCase = _cases[value];
            }
        }

        public string CurrentCaseFileName
        {
            get
            {
                return _caseFiles.ContainsKey(CurrentCase) ? _caseFiles[CurrentCase] : "<безымянный проект>";
            }
        }

        public void addCase(Case c)
        {
            _cases.Add(c);
            RaiseAddCaseEvent(this, new CaseEventArgs(c));

            CurrentCase = c;
        }

        public void removeCase()
        {
            int ci = 0;
            if (CurrentCase != null)
            {

                string messageBoxText = "Сохранить изменения перед закрытием?";
                string caption = "Сохранение";
                MessageBoxButton button = MessageBoxButton.YesNo;
                MessageBoxImage icon = MessageBoxImage.Warning;
                MessageBoxResult result = MessageBox.Show(messageBoxText, caption, button, icon);
                if (result == MessageBoxResult.Yes)
                {
                    // if we want to save
                    if (!saveCaseAs()) // but saving is failed
                        return;
                }

                ci = CurrentCaseIndex;
                RaiseBeforeRemoveCaseEvent(this, new CaseEventArgs(CurrentCase));
                _cases.Remove(CurrentCase);
                _caseFiles.Remove(CurrentCase);
            }
            if (_cases.Count > 0)
            {
                CurrentCaseIndex = Math.Max(--ci, 0);
            }
            else
            {
                CurrentCase = null;
            }
        }

        private bool GetNewFileName()
        {
            Microsoft.Win32.SaveFileDialog saveFileDialog1 = new Microsoft.Win32.SaveFileDialog();
            saveFileDialog1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == true)
            {
                _caseFiles[CurrentCase] = saveFileDialog1.FileName;
                return true;
            }
            return false;
        }

        public bool saveCase()
        {
            
            if (!_caseFiles.ContainsKey(CurrentCase))
            {
                if (!GetNewFileName())
                    return false;
            }
            string filepath = _caseFiles[CurrentCase];
            
            System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(Case));
            System.IO.StreamWriter file = new System.IO.StreamWriter(filepath);
            writer.Serialize(file, CurrentCase);
            file.Close();
            return true;
        }

        public bool saveCaseAs()
        {
            return GetNewFileName() && saveCase();
        }

        public void openCase()
        {
            Microsoft.Win32.OpenFileDialog openFileDialog1 = new Microsoft.Win32.OpenFileDialog();

            openFileDialog1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == true && !_caseFiles.ContainsValue(openFileDialog1.FileName))
            {
                try
                {
                    System.Xml.Serialization.XmlSerializer reader =
                        new System.Xml.Serialization.XmlSerializer(typeof(Case));
                    System.IO.StreamReader file = new System.IO.StreamReader(openFileDialog1.FileName);
                    Case newCase = (Case)reader.Deserialize(file);
                    file.Close();

                    addCase(newCase);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        // about events: https://msdn.microsoft.com/ru-ru/library/w369ty8x.aspx

        //private void PrintPageHandler(object sender, PrintPageEventArgs e)
        //{
        //    //DataTable dt = new DataTable("Customers");
        //    //dt.Columns.Add(new DataColumn("LastName"));
        //    //dt.Columns.Add(new DataColumn("FirstName"));

        //    //// Populate the table 
        //    //dt.Rows.Add("Baggins", "Bilbo");


        //    //DataGrid dg = new DataGrid();
        //    //dg.

        //    Calculations calcs = new Calculations(CurrentCase);
        //    calcs.Measure(new Size(calcs.Width, calcs.Height));
        //    calcs.Arrange(new Rect(new Size(calcs.Width, calcs.Height)));
        //    RenderTargetBitmap rtbm = new RenderTargetBitmap((int)calcs.Width, (int)calcs.Height, 
        //                                                        96, 96, PixelFormats.Pbgra32);
        //    rtbm.Render(calcs);

        //    System.IO.MemoryStream stream = new System.IO.MemoryStream();
        //    BitmapEncoder encoder = new BmpBitmapEncoder();
        //    encoder.Frames.Add(BitmapFrame.Create(rtbm));
        //    encoder.Save(stream);

        //    System.Drawing.Bitmap bm = new System.Drawing.Bitmap(stream);

        //    e.Graphics.DrawImage(bm, 0, 0);
        //}

        public void printCase()
        {
            PrintDialog pDialog = new PrintDialog();
            pDialog.PageRangeSelection = PageRangeSelection.AllPages;
            pDialog.UserPageRangeEnabled = true;
            
            Nullable<Boolean> print = pDialog.ShowDialog();
            if (print == true)
            {
                var hMargin = 30;
                var vMargin = 15;
                var w = pDialog.PrintableAreaWidth - hMargin * 2;
                var h = pDialog.PrintableAreaHeight - vMargin * 2;
                Calculations calcs = new Calculations(CurrentCase);

                calcs.ResultsContainer.Measure(new Size(w, h));
                calcs.ResultsContainer.Arrange(new Rect(new Point(hMargin, vMargin), new Size(w, h)));

                pDialog.PrintVisual(calcs.ResultsContainer, "Sputnik Angosat: Результаты расчета");
            }
        }
    }
}
