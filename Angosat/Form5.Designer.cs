﻿namespace Angosat
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.T_total_pr_sr_input = new System.Windows.Forms.TextBox();
            this.panel_equivalent_noise_bandwidth = new System.Windows.Forms.Panel();
            this.T_total_pr_sr_label = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel_equivalent_noise_bandwidth.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // T_total_pr_sr_input
            // 
            this.T_total_pr_sr_input.Location = new System.Drawing.Point(348, 0);
            this.T_total_pr_sr_input.Name = "T_total_pr_sr_input";
            this.T_total_pr_sr_input.Size = new System.Drawing.Size(148, 20);
            this.T_total_pr_sr_input.TabIndex = 19;
            this.T_total_pr_sr_input.Text = "478,63";
            // 
            // panel_equivalent_noise_bandwidth
            // 
            this.panel_equivalent_noise_bandwidth.AutoScroll = true;
            this.panel_equivalent_noise_bandwidth.AutoSize = true;
            this.panel_equivalent_noise_bandwidth.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_equivalent_noise_bandwidth.Controls.Add(this.T_total_pr_sr_label);
            this.panel_equivalent_noise_bandwidth.Controls.Add(this.T_total_pr_sr_input);
            this.panel_equivalent_noise_bandwidth.Location = new System.Drawing.Point(3, 3);
            this.panel_equivalent_noise_bandwidth.Name = "panel_equivalent_noise_bandwidth";
            this.panel_equivalent_noise_bandwidth.Size = new System.Drawing.Size(499, 23);
            this.panel_equivalent_noise_bandwidth.TabIndex = 24;
            // 
            // T_total_pr_sr_label
            // 
            this.T_total_pr_sr_label.AutoSize = true;
            this.T_total_pr_sr_label.Location = new System.Drawing.Point(3, 0);
            this.T_total_pr_sr_label.Name = "T_total_pr_sr_label";
            this.T_total_pr_sr_label.Padding = new System.Windows.Forms.Padding(2);
            this.T_total_pr_sr_label.Size = new System.Drawing.Size(315, 17);
            this.T_total_pr_sr_label.TabIndex = 16;
            this.T_total_pr_sr_label.Text = "Эквивалентная шумовая температура всей приемной СР, K";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.panel_equivalent_noise_bandwidth);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(17, 17);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(8);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(505, 29);
            this.flowLayoutPanel1.TabIndex = 27;
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(560, 223);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "Form5";
            this.Text = "КА приемника АФУ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form5_FormClosing);
            this.panel_equivalent_noise_bandwidth.ResumeLayout(false);
            this.panel_equivalent_noise_bandwidth.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel_equivalent_noise_bandwidth;
        private System.Windows.Forms.Label T_total_pr_sr_label;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TextBox T_total_pr_sr_input;

    }
}