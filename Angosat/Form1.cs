﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace Angosat
{
    public partial class Form1 : Form
    {
        StreamReader streamToPrint;
        Font printFont;
        public List<InputDataForm> forms = new List<InputDataForm>();

        public Form1()
        {
            InitializeComponent();
        }

        public void CreateNewDocument_Click(object sender, EventArgs e)
        {
            InputDataForm inputDataForm = new InputDataForm();
            TabPage page = inputDataForm.createTabPage(this);
            tabControl1.TabPages.Add(page);
        }

        public void toolStripButton1_Click(object sender, EventArgs e)
        {
            InputDataForm inputDataForm = new InputDataForm();
            TabPage page = inputDataForm.createTabPage(this);
            tabControl1.TabPages.Add(page);
            tabControl1.SelectedTab = page;
        }

        public void CloseToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab != null)
                tabControl1.SelectedTab.Dispose();
        }

        public void closeTab_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab != null)
            {
                tabControl1.SelectedTab.Dispose();
            }
        }

        public void CBandToggle_Click(object sender, EventArgs e)
        {
            CBandToggle.Checked = true;
            KuBandToggle.Checked = false;
            KuTVBandToggle.Checked = false;
        }

        private void KuBandToggle_Click(object sender, EventArgs e)
        {
            CBandToggle.Checked = false;
            KuBandToggle.Checked = true;
            KuTVBandToggle.Checked = false;
        }

        public void KuTVBandToggle_Click(object sender, EventArgs e)
        {
            CBandToggle.Checked = false;
            KuBandToggle.Checked = false;
            KuTVBandToggle.Checked = true;
        }

        public void button1_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2(CBandToggle.Checked, tabControl1.SelectedTab);
            form2.ShowDialog();
        }

        public void button2_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3(CBandToggle.Checked, tabControl1.SelectedTab);
            form3.ShowDialog();
        }

        public void button3_Click(object sender, EventArgs e)
        {
            Form4 form4 = new Form4(tabControl1.SelectedTab);
            form4.ShowDialog();
        }

        public void button4_Click(object sender, EventArgs e)
        {
            Form5 form5 = new Form5(CBandToggle.Checked, tabControl1.SelectedTab);
            form5.ShowDialog();
        }

        private void openDialogToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ReadFile();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            ReadFile();
        }

        private void ReadFile()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    int count = tabControl1.TabPages.Count;
                    for (int i = 0; i < count; i++)
                    {
                        if (tabControl1.TabPages[i].Text.Equals(openFileDialog1.FileName))
                        {
                            tabControl1.SelectedTab = tabControl1.TabPages[i];
                            return;
                        }
                    }
                    openFile(openFileDialog1.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void openFile(String fileName)
        {
            Evaluations ev = Evaluations.ReadXML(fileName);
            InputDataForm inputDataForm = new InputDataForm();
            TabPage page = inputDataForm.createTabPage(this);
            page.Text = fileName;
            tabControl1.TabPages.Add(page);
            tabControl1.SelectedTab = page;
            Evaluations.setEvals(tabControl1.SelectedTab, ev);
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            saveFile();
        }

        private void tabControl1_DoubleClick(object sender, EventArgs e)
        {
            TabPage page = tabControl1.SelectedTab;
            if (page != null)
            {
                tabControl1.TabPages.Remove(page);
            }
        }

        private void saveToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            saveFile();
        }

        private void saveFile()
        {
            TabPage tabPage = tabControl1.SelectedTab;
            if (tabPage == null)
                return;

            String filename = tabPage.Text;
            if (!filename.Equals("Новый"))
            {
                Evaluations.WriteXML(Evaluations.getEvals(tabControl1.SelectedTab), filename);
            }
            else
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
                saveFileDialog1.FilterIndex = 1;
                saveFileDialog1.RestoreDirectory = true;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    Evaluations.WriteXML(Evaluations.getEvals(tabControl1.SelectedTab), saveFileDialog1.FileName);
                    tabControl1.SelectedTab.Text = saveFileDialog1.FileName;

                }
            }
        }

        private void saveAsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            TabPage tabPage = tabControl1.SelectedTab;
            if (tabPage == null)
                return;


            String filename = tabPage.Text;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Evaluations.WriteXML(Evaluations.getEvals(tabControl1.SelectedTab), saveFileDialog1.FileName);
                tabControl1.SelectedTab.Text = saveFileDialog1.FileName;

            }
        }

        private void выходToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            getResults();
        }

        private void getResults()
        {
            TabPage tabPage = tabControl1.SelectedTab;
            Form6.Band selectedBand = CBandToggle.Checked ? Form6.Band.C : (KuBandToggle.Checked ? Form6.Band.Ku : Form6.Band.KuTV);
            Form6 form = new Form6(selectedBand, tabPage);
            form.ShowDialog();
        }

        private void getResultsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            getResults();
        }

        private void печатьToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            // Declare a string to hold the entire document contents.
            string documentContents;
            string stringToPrint;


            List<Angosat.Form6.Row> rows;

            Form6.Band selectedBand = CBandToggle.Checked ? Form6.Band.C : (KuBandToggle.Checked ? Form6.Band.Ku : Form6.Band.KuTV);
            switch (selectedBand)
            {
                case Form6.Band.C: rows = Form6.evalDataC(tabControl1.SelectedTab); break;
                case Form6.Band.Ku: rows = Form6.evalDataKu(tabControl1.SelectedTab); break;
                case Form6.Band.KuTV: rows = Form6.evalDataKuTV(tabControl1.SelectedTab); break;
                default: rows = new List<Angosat.Form6.Row>(); break;
            }

            String contents = "";
            foreach (Angosat.Form6.Row row in rows)
            {
                contents = contents + row.ToString() + "\n";
            }


            string path = @"c:\temp\MyFile.txt";

            if (Directory.Exists(@"c:\temp"))
            {
                Console.WriteLine("That path exists already.");
                return;
            }

            // Try to create the directory.
            DirectoryInfo di = Directory.CreateDirectory(@"c:\temp");
            // This text is added only once to the file. 
            if (!File.Exists(path))
            {
                File.WriteAllText(path, contents);
            }
        }

        private void pd_PrintFile(object sender, EventArgs e)
        {
            try
            {
                string path = @"c:\temp\MyFile.txt";

                streamToPrint = new StreamReader(path);
                try
                {
                    printFont = new Font("Arial", 10);
                    PrintDocument pd = new PrintDocument();
                    pd.PrintPage += new PrintPageEventHandler
                       (this.pd_PrintPage);
                    pd.Print();
                }
                finally
                {
                    streamToPrint.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        // The PrintPage event is raised for each page to be printed.
        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            string line = null;

            // Calculate the number of lines per page.
            linesPerPage = ev.MarginBounds.Height /
               printFont.GetHeight(ev.Graphics);

            // Print each line of the file.
            while (count < linesPerPage &&
               ((line = streamToPrint.ReadLine()) != null))
            {
                yPos = topMargin + (count *
                   printFont.GetHeight(ev.Graphics));
                ev.Graphics.DrawString(line, printFont, Brushes.Black,
                   leftMargin, yPos, new StringFormat());
                count++;
            }

            // If more lines exist, print another page.
            if (line != null)
                ev.HasMorePages = true;
            else
                ev.HasMorePages = false;
        }

    }
}

