﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Angosat
{
    public partial class Form3 : Form
    {
        public TabPage tab;
        public Boolean isC = false;

        public Form3(Boolean isC, TabPage tabPage)
        {
            tab = tabPage;
            this.isC = isC;
            InitializeComponent();
            LoadData();

            if (isC)
            {
                this.panel_attenuation_due_to_rain.Hide();
                this.F_sh_pr_panel.Hide();
            }
        }

        private void LoadData()
        {
            Evaluations evals = Evaluations.getEvals(tab);

            this.input_antenna_angle.Text = evals.r_orientationCoefficient.ToString();
            this.input_attenuation_due_to_rain.Text = evals.r_attenuation_due_to_rain.ToString();
            this.input_operating_frequency.Text = evals.r_operating_frequency.ToString();
            this.input_antenna_diameter.Text = evals.r_antenna_diameter.ToString();
            this.input_station_longitude.Text = evals.r_satellite_longitude.ToString();
            this.input_satellite_longitude.Text = evals.r_station_longitude.ToString();
            this.input_station_latitude.Text = evals.r_station_latitude.ToString();
            this.input_atmospheric_loss.Text = evals.r_L_atm_pr_czs.ToString();
            this.n_pr_czs_input.Text = evals.n_pr_czs.ToString();
            this.fec_input.Text = evals.FEC_pr_czs.ToString();
            this.Rb_pr_czs_input.Text = evals.Rb_pr_czs.ToString();
            this.F_sh_pr_input.Text = evals.F_sh_pr_tv.ToString();

            this.input_effective_temperature_at_the_LNA_input.Text = evals.r_effective_temperature_at_the_LNA_input.ToString();
            this.input_angle_between_sun_and_ZS.Text = evals.r_angle_between_sun_and_ZS.ToString();
            //this.input_noise_equivalent_bandwidth.Text = evals.r_noise_equivalent_bandwidth.ToString();
        }

        private void Form3_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Double antenna_angle = Evaluations.parseDouble(this.input_antenna_angle, this.label_antenna_angle);
                Double attenuation_due_to_rain = Evaluations.parseDouble(this.input_attenuation_due_to_rain, this.label_attenuation_due_to_rain);
                Double operating_frequency = Evaluations.parseDouble(this.input_operating_frequency, this.label_operating_frequency);

                if (isC)
                    operating_frequency = Evaluations.parseDouble(this.input_operating_frequency, this.label_operating_frequency, 3570.0, 6355.0, true);
                else
                    operating_frequency = Evaluations.parseDouble(this.input_operating_frequency, this.label_operating_frequency, 10990.0, 14210.0, true);

                Double antenna_diameter = Evaluations.parseDouble(this.input_antenna_diameter, this.label_antenna_diameter);
                Double satellite_longitude = Evaluations.parseDouble(this.input_station_longitude, this.label_satellite_longitude);
                Double station_longitude = Evaluations.parseDouble(this.input_satellite_longitude, this.label_station_longitude);
                Double station_latitude = Evaluations.parseDouble(this.input_station_latitude, this.label_station_latitude);

                Double effective_temperature_at_the_LNA_input = Evaluations.parseDouble(this.input_effective_temperature_at_the_LNA_input, this.label_effective_temperature_at_the_LNA_input);
                Double angle_between_sun_and_ZS = Evaluations.parseDouble(this.input_angle_between_sun_and_ZS, this.label_angle_between_sun_and_ZS);
                //Double noise_equivalent_bandwidth = Evaluations.parseDouble(this.input_noise_equivalent_bandwidth, this.label_noise_equivalent_bandwidth);

                Evaluations evals = Evaluations.getEvals(tab);
                evals.r_orientationCoefficient = antenna_angle;
                evals.r_attenuation_due_to_rain = attenuation_due_to_rain;
                evals.r_operating_frequency = operating_frequency;
                evals.r_antenna_diameter = antenna_diameter;
                evals.r_satellite_longitude = satellite_longitude;
                evals.r_station_longitude = station_longitude;
                evals.r_station_latitude = station_latitude;
                evals.r_L_atm_pr_czs = Evaluations.parseDouble(this.input_atmospheric_loss, this.label_atmospheric_loss);

                evals.r_effective_temperature_at_the_LNA_input = effective_temperature_at_the_LNA_input;
                evals.r_angle_between_sun_and_ZS = angle_between_sun_and_ZS;

                evals.n_pr_czs = Evaluations.parseDouble(this.n_pr_czs_input, this.n_pr_czs_label);
                evals.FEC_pr_czs = Evaluations.parseDouble(this.fec_input, this.fec_label);
                evals.Rb_pr_czs = Evaluations.parseDouble(this.Rb_pr_czs_input, this.Rb_pr_czs_label);
                evals.F_sh_pr_tv = Evaluations.parseDouble(this.F_sh_pr_input, this.F_sh_pr_label);
            }
            catch (Exception ex)
            {
                MessageBox.Show("У вас ошибка в поле " + ex.Message,
                    "Исключительная ситуация",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button1);
                e.Cancel = true;
            }
        }
    }
}
