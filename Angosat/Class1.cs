﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Angosat
{
    [Serializable]
    public class Evaluations
    {
        public static void WriteXML(Evaluations ev, String filepath)
        {
            System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(Evaluations));
            System.IO.StreamWriter file = new System.IO.StreamWriter(filepath);
            writer.Serialize(file, ev);
            file.Close();
        }

        public static Evaluations ReadXML(String filepath)
        {
            Evaluations ev;
            System.Xml.Serialization.XmlSerializer reader =
                new System.Xml.Serialization.XmlSerializer(typeof(Evaluations));
            System.IO.StreamReader file = new System.IO.StreamReader(filepath);
            ev = (Evaluations)reader.Deserialize(file);
            file.Close();
            return ev;
        }

        private static int lenght = 10;

        private static Dictionary<TabPage, Evaluations> dict = new Dictionary<TabPage, Evaluations>();
        private static Evaluations[] singletonArray = new Evaluations[lenght];
        public static Evaluations getEvals(TabPage page)
        {
            if (!dict.ContainsKey(page))
                dict.Add(page, new Evaluations());

            return dict[page];
        }

        public static void remove(TabPage page)
        {
            dict.Remove(page);
        }

        public static void setEvals(TabPage page, Evaluations e)
        {
            if (dict.ContainsKey(page))
                dict.Remove(page);

            dict.Add(page, e);
        }

        // consts
        private const double geostationary_altitude = 35786000;
        private const double earth_radius = 6378000;
        private const double K11 = 6.63;
        private const double light_speed = 3e8;  // скорость Света
        private const double utilization_antenna_surface = 0.7;  // ku - коэффициент использования поверхности антенны
        private const double L_pol = 0.07;
        private const double L_aft_pr_czs = 0.15;
        private const double L_aft_per_czs = 3;
        private const double k = 1.38e-23;
        private const double C_BAND__G_pr_sr= 22;  // коэффициент усиления приемник антенна СР
        private const double KU_BAND__G_pr_sr = 27;  // коэффициент усиления приемник антенна СР
        // GT.пр.ср
        private const double KU_BAND__GT_pr_sr = 2.1;
        private const double C_BAND__GT_pr_sr = -6;  
        private const double T_atm_pr_czs = 270;  // температура окружающей атмосферы
        private const double T_g_pr_czs = 2.7;
        private const double T_s_pr_czs = 15;  // T.с.пр.цзс
        private const double T0_pr_czs = 290;  // T0.пр.цзс
        // Р.ум.пер.цзс - мощность на выходе усилителя
        private const double C_BAND__P_ym_per_czs = 1000; 
        private const double KU_BAND__P_ym_per_czs = 400;
        // мощность на выходе усилителя при насыщении
        private const double C_BAND__YM_nasish_per_czs = 1100;
        private const double KU_BAND__YM_nasish_per_czs = 500;

        private const double effective_temperature = 260;

        private static Double PI = 3.14;  // for testing

        // only KuTV
        private const double alpha_tv = 0.3;
        private const double R1 = 75;
        private const double T_s_tv = 290;
        
        // VARIABLES 
        // ЗС передатчика АФУ
        // C/Ku common
        public double s_orientationCoefficient = 1.8;
        public Double s_station_latitude = 8.5;
        public Double s_satellite_longitude = 14.5;
        public Double s_station_longitude = 13.33;
        public Double s_antenna_diameter = 9;  // D.пер.цзс
        public Double s_operating_frequency = 6355;  // f.пер.цзс
        // Количество битов на один симбол
        public Double n_per_czs = 2; // бит/с
        public Double FEC_per_czs = 7.0/8.0;
        // скорость передачи символов
        public Double Rb_per_czs = 85;
        public Double rolloff_per_czs = 1.35;
        // Ku ONLY
        public Double s_attenuation_due_to_rain = 12.93;
        public Double s_atmospheric_loss = 0.13;
        
        // Транспондер приемника
        // TΣ.пр.ср
        public Double C_BAND__T_total_pr_sr = 501.18;
        public Double KU_BAND__T_total_pr_sr = 478.63;
        // Транспондер передатчика
        // TODO: =49.29 - for Ku
        public Double equivalent_isotropic_radiated_power = 39.86;  // ЭИИМ.ср
        // ЗС приемника АФУ
        // common
        public double r_orientationCoefficient = 2.8;
        public Double r_station_latitude = 8.5;
        public Double r_station_longitude = 14.5;
        public Double r_satellite_longitude = 13.33;
        public Double r_antenna_diameter = 9;
        public Double r_operating_frequency = 4130;
        public Double r_L_atm_pr_czs = 10 * Math.Log10(1.33);
        public Double r_effective_temperature_at_the_LNA_input = 150;
        public Double r_angle_between_sun_and_ZS = 72.71;
        public Double n_pr_czs = 2; // бит/с
        public Double FEC_pr_czs = 7.0 / 8.0;
        public Double Rb_pr_czs = 85;
        public Double rolloff_pr_czs = 1.35;
        public Double r_koefficient_margin = 7;
        // Ku only
        public Double r_attenuation_due_to_rain = 7.18; // A.001.пр.цзс

        // KuTV only
        public double F_sh_pr_tv = 1.5;

        public static Double toDeg(Double value) { return value * 180.0 / Math.PI; }
        public static Double toRad(Double value) { return value / 180.0 * Math.PI; }
        public static Double DegCos(Double deg) { return Math.Cos(toRad(deg)); }
        public static Double DegSin(Double deg) { return Math.Sin(toRad(deg)); }

        // from http://www.johndcook.com/blog/cpp_erf/
        public static double erf(double x)
        {
            // constants
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;
            double p = 0.3275911;

            // Save the sign of x
            int sign = 1;
            if (x < 0)
                sign = -1;
            x = Math.Abs(x);

            // A&S formula 7.1.26
            double t = 1.0 / (1.0 + p * x);
            double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);

            return sign * y;
        }

        public static double erfc(double x)
        {
            return 1.0 - erf(x);
        }

        // C
        public Double f1_1 ()
        {
            Double x1 = DegCos (s_station_latitude);
            Double x2 = DegCos (s_satellite_longitude - s_station_longitude);
            Double d = Math.Sqrt (geostationary_altitude * geostationary_altitude + 
                2.0 * earth_radius * (geostationary_altitude + earth_radius) *
                ((1.0 - x1) * x2)) 
                / 1000.0;
            return d;
        }

        public Double f1_2 ()
        {
            Double x = K11 * DegCos(s_satellite_longitude - s_station_longitude) * DegCos(s_station_latitude) - 1;  // Correct
            Double y = Math.Sqrt(K11 * K11 + 1 - 2 * K11 * DegCos(s_satellite_longitude - s_station_longitude) * DegCos(s_station_latitude));   // Correct
            Double r = toDeg(Math.Asin (x / y));
            return r;
        }

        public Double f1_3 ()
        {
            Double la1 = DegSin (s_station_latitude);
            Double cx = DegCos (s_satellite_longitude - s_station_longitude);
            Double bx = earth_radius / (earth_radius + geostationary_altitude);
            Double x = la1 * (1.0 - bx * cx);
            Double y = Math.Sqrt (1.0 - cx * cx);
            Double la2 = DegCos (s_station_latitude);
            Double z = Math.Sqrt (1.0 - 2.0 * bx * cx + bx * bx * la2 * la2);
            Double r = x / (y * z);
            return toDeg(Math.Cos (x / (y * z)));
        }

        public Double f1_4 ()
        {
            return 70.0 * light_speed / (s_operating_frequency * s_antenna_diameter * 1e6);
        }

        public Double f1_5 ()
        {
            return 9.94 +
                10 * Math.Log10 (utilization_antenna_surface) +
                20 * Math.Log10 (s_operating_frequency *1e6 * s_antenna_diameter / light_speed);
        }

        public Double f1_6 ()  // =f2_7
        {
            return 32.4 + 
                20 * Math.Log10(s_operating_frequency) +
                20 * Math.Log10(f1_1());
        }

        public Double f1_7 ()  // =f2_9
        {
            return f1_4 () / s_orientationCoefficient;
        }

        public Double f1_8 ()
        {
            return 12.0 * Math.Pow (f1_7 () / f1_4 (), 2);
        }

        public Double f1_9 ()
        {
            return L_pol + f1_8 ();
        }

        public Double f1_10()  // =f2_12  L.макс.пер.цзс
        {
            return f1_9 () + f1_6 ();
        }

        public Double f1_11 ()
        {
            return f1_5 () - f1_8 ();
        }

        //public Double f1_12 ()
        //{
        //    double a1 = Math.Pow (10, loss_in_AFT_sr / 10);

        //    double x = noise_temp_antenna_satellite / a1;
        //    double y = (a1 - 1) * absolute_temparature / a1;

        //    double a2 = Math.Pow (10, s_noise_factor / 10);
        //    double z = absolute_temparature * (a2 - 1);
        //    return x + z + y;
        //}

        public Double f1_13 ()
        {
            return 10 * Math.Log10(k * C_BAND__T_total_pr_sr * f1_13NA());
        }

        public Double R_c_per_czs()
        {
            return Rb_per_czs * 1e6 / (FEC_per_czs * n_per_czs);
        }

        public Double f1_13NA()  // =2.16NA; Δf.ш.ср
        {
            return R_c_per_czs() * rolloff_per_czs;
        }

        public Double f1_14 ()
        {
            return 10 * Math.Log10(k * C_BAND__T_total_pr_sr);
        }

        public Double f1_15()  // Р.пер.цзс
        {
            return 10 * Math.Log10(C_BAND__P_ym_per_czs) + f1_5() - L_aft_per_czs - f1_15NA();
        }

        public Double f1_15NA() // OBO1.пер.цзс
        {
            return 10 * Math.Log10(C_BAND__P_ym_per_czs / C_BAND__YM_nasish_per_czs);
        }

        public Double f1_16 ()
        {
            // 
            return f1_15 ()  - 20 * Math.Log10 (f1_1 () * 1000) - 10 * Math.Log10 (4 * PI);
        }

        public Double f1_17 ()
        {
            return f1_15 () - 20 * Math.Log10 (f1_1 ()) + 74.8;
        }

        public Double f1_17NA()
        {
            return f1_15() + C_BAND__G_pr_sr - f1_6();
        }

        public Double f1_18 ()
        {
            return f1_15() +
                C_BAND__GT_pr_sr -
                10 * Math.Log10(k * f1_13NA()) - 
                f1_10();
        }

        public Double f1_19 ()
        {
            return f1_18() + 10 * Math.Log10(f1_13NA());
        }

        public Double f1_20 ()
        {
            return f1_19 () + 10 * Math.Log10 (k);
        }

        public Double f1_20NA()  // EbN0.пер.цзс
        {
            return f1_19() - 10 * Math.Log10(Rb_per_czs * 1e6);
        }

        // BHu3
        public Double f1_21 ()
        {
            return Math.Sqrt(geostationary_altitude * geostationary_altitude +
                             2.0 * earth_radius * 
                             (geostationary_altitude + earth_radius) * 
                             (1.0 - DegCos (r_station_latitude)) * 
                             DegCos (r_station_longitude - r_satellite_longitude)) * 1e-3;
        }

        public double f1_21NA()  // Г.пер.цзс
        {
            return Rb_per_czs * 1e6 / f1_13NA();
        }

        public Double f1_22 ()
        {
            Double x = K11 * DegCos (r_station_latitude) * DegCos (r_satellite_longitude - r_station_longitude) - 1;
            Double y = Math.Sqrt (K11 * K11 - 2 * K11 * DegCos (r_satellite_longitude - r_station_longitude) * DegCos (r_station_latitude) + 1);
            Double r = toDeg(Math.Asin(x / y));
            return r;
        }

        public Double f1_23 ()
        {
            Double la1 = DegSin (r_station_latitude);
            Double cx = DegCos (r_satellite_longitude - r_station_longitude);
            Double bx = earth_radius / (earth_radius + geostationary_altitude);
            Double x = la1 * (1.0 - bx * cx);
            Double y = Math.Sqrt (1.0 - cx * cx);
            Double la2 = DegCos (r_station_latitude);
            Double z = Math.Sqrt (1.0 - 2.0 * bx * cx + bx * bx * la2 * la2);
            return toDeg(Math.Cos(x / (y * z)));
        }

        public Double f1_24()
        {
            return 70.0 * light_speed / (r_operating_frequency * r_antenna_diameter * 1e6);
        }

        public Double f1_25()  // =f2_34
        {
            return 9.94 + 10 * Math.Log10(utilization_antenna_surface) + 
                   20 * Math.Log10 (r_operating_frequency * 1e6 * r_antenna_diameter / light_speed);
        }


        public Double f1_26 ()
        {
            return Math.Pow (10, f1_25 () / 10) * light_speed * light_speed / 
                   (4 * PI * r_operating_frequency * 1e6 * 1e6 * r_operating_frequency);
        }

        public Double f1_27()  // L.св.пр.цзс
        {
            return 32.4 + 20 * Math.Log10(r_operating_frequency) + 20 * Math.Log10(f1_21());
        }

        public Double f1_28 ()
        {
            return f1_24() / r_orientationCoefficient;
        }

        public Double f1_29 ()
        {
            return 12f * Math.Pow( f1_28() / f1_24(), 2);
        }

        public Double f1_30 ()
        {
            return L_pol + f1_29 ();
        }

        public Double f1_31 ()
        {
            return f1_30 () + f1_27();
        }

        public Double f1_32 ()
        {
            return f1_25 () - f1_29 ();
        }

        public Double f1_33NA()  // Ta0.пр.цзс
        {
            double a1 = Math.Pow(10, -0.1 * r_L_atm_pr_czs);  // temp var
            return (1 - a1) * T_atm_pr_czs + a1 * T_g_pr_czs;
        }

        public Double f1_33 ()
        {
            return 10 * Math.Log10(1 + r_effective_temperature_at_the_LNA_input / T0_pr_czs);
        }

        public Double f1_34 ()
        {
            double laft = 1.0 / Math.Pow(10, L_aft_pr_czs / 10);
            return f1_33NA() * laft + (1 - laft) * T0_pr_czs;
        }

        public Double f1_35 ()
        {
            return f1_34() + T0_pr_czs * (Math.Pow(10, 0.1 * f1_33()) - 1);
        }

        public double R_c_pr_czs()
        {
            return Rb_pr_czs * 1e6 / (FEC_pr_czs * n_pr_czs);
        }

        public Double f1_36NA()  // =2.48NA; Δf.ш.цзс
        {
            return R_c_pr_czs() * rolloff_pr_czs;
        }

        public Double f1_36 ()
        {
            return 10* Math.Log10(k * f1_35() * f1_36NA());
        }

        public Double f1_37 ()
        {
            return 10* Math.Log10(k * f1_35 ());
        }

        public Double f1_38 ()
        {
            return equivalent_isotropic_radiated_power - f1_27() + f1_25 () - f1_30 ();
        }

        public Double f1_39 ()
        {
            return equivalent_isotropic_radiated_power - 20 * Math.Log10 (f1_21 () * 1000) - 10 * Math.Log10 (4 * PI);
        }

        public Double f1_40 ()
        {
            return f1_38 () - 20 * Math.Log10 (f1_21 ()) + 74.8;
        }

        public Double f1_41()  // GT.пр.цзс
        {
            return f1_25 () - 10 * Math.Log10 (f1_35());
        }

        public Double f1_41NA()
        {
            return equivalent_isotropic_radiated_power + f1_25() - f1_27();
        }

        public Double f1_42 ()
        {
            return equivalent_isotropic_radiated_power +
                   f1_41 () -
                   10 * Math.Log10(f1_36NA() * k) -
                   f1_31 ();
        }

        public Double f1_43 ()
        {
            return equivalent_isotropic_radiated_power + 
                   f1_41 () -
                   10 * Math.Log10 (k) -
                   f1_31 ();
        }

        public Double f1_43NA()  // EbN0.пр.цзс
        {
            return f1_43() - 10 * Math.Log10(Rb_pr_czs * 1e6);
        }

        public Double f1_44 ()
        {
            return f1_43 () + 10 * Math.Log10 (k);
        }

        public Double f1_45 ()
        {
            return 10 * Math.Log10 (1 / (1 / Math.Pow (10, f1_18 () * 0.1)
                                       + 1 / Math.Pow (10, f1_42 () * 0.1)));
        }

        public Double f1_46 ()
        {
            return 10 * Math.Log10 (1 / 
                (1 / Math.Pow (10, f1_19 () * 0.1) + 
                1 / Math.Pow (10, f1_43 () * 0.1)));
        }

        public Double f1_46NA()
        {
            return -10 * Math.Log10(Math.Pow(10, -0.1 * f1_20NA()) + Math.Pow(10, -0.1 * f1_43NA()));
        }

        public Double f1_47 ()
        {
            return 0.5 * 120000 / Math.Pow (r_operating_frequency * 1e-3, 0.75);
        } 

        public Double f1_48 ()
        {
            return 0.036 / DegSin (r_angle_between_sun_and_ZS);
        }

        public Double f1_49 ()
        {
            return f1_47 () * Math.Pow (10, -0.1 * f1_48 ());
        }

        public Double f1_50 ()
        {
            return f1_49 () * Math.Pow (0.53 / f1_24 (), 2f);
        }

        public Double f1_51 ()
        {
            return 0.5 * f1_49 ();
        }

        public Double f1_52 ()
        {
            return (f1_24 () + 0.48) / 0.4;
        }

        public Double f1_53 ()
        {
            return (f1_24 () + 0.48) / 0.25;
        }

        public Double f1_54 ()
        {
            return PI * Math.Pow (f1_24 () + 0.48, 2) / (0.25 * 4 * 0.4);
        }

        public Double f1_55 ()
        {
            return 10 * Math.Log10 ((f1_35 () + f1_50 ()) / f1_35 ());
        }


        public double f1_56()  // M.пр
        {
            return equivalent_isotropic_radiated_power + f1_41() - f1_43NA() -
                   10 * Math.Log10(Rb_pr_czs * 1e6) - 10 * Math.Log10(k) - f1_27();
        }

        public double f1_57()  // Г.пр.цзс
        {
            return Rb_pr_czs * 1e6 / f1_36NA();
        }

        public double f1_58()  // P.в.пр.цзс
        {
            return erfc(Math.Sqrt(f1_43NA()));
        }

        // Ku only formulas
        public Double f2_6 ()
        {
            return s_attenuation_due_to_rain + s_atmospheric_loss;
        }

        public Double f2_8 ()
        {
            return f1_6 () + f2_6();
        }

        public Double f2_13()  // L.макс.пер.цзс.д
        {
            return f2_8 () + f1_9 () + f2_6();
        }

        public Double f2_15()
        {
            return KU_BAND__T_total_pr_sr;
        }

        public Double f2_16 ()
        {
            return 10 * Math.Log10(k * KU_BAND__T_total_pr_sr * f1_13NA());
        }

        public Double f2_17 (){
            return 10 * Math.Log10 (k * KU_BAND__T_total_pr_sr);
        }

        public Double f2_17NA() // OBO1.пер.цзс
        {
            return 10 * Math.Log10(KU_BAND__P_ym_per_czs / KU_BAND__YM_nasish_per_czs);
        }

        public Double f2_18NA () 
        {
            return 10 * Math.Log10(KU_BAND__P_ym_per_czs) + f1_5() - L_aft_per_czs - f2_17NA();
        }

        public Double f2_20() {
            return f2_18NA () - 20 * Math.Log10 (f1_1 () * 1000) - 10 * Math.Log10 (4 * PI);
        }

        public Double f2_22 ()
        {
            return f2_18NA () - 20 * Math.Log10 (f1_1 ()) + 74.8;
        }

        public Double f2_23NA()  // C.пер.цзс
        {
            return f2_18NA() + KU_BAND__G_pr_sr - f1_10();
        }

        public Double f2_24()  // CN.пер.цзс
        {
            return f2_18NA() + KU_BAND__GT_pr_sr - 10 * Math.Log10(k * f1_13NA()) - f1_10();
        }

        public Double f2_24NB()  // C.пер.цзс.д
        {
            return f2_18NA() + KU_BAND__G_pr_sr - f2_13();
        }

        public Double f2_25()  // CN.пер.цзс.д
        {
            return f2_18NA() + KU_BAND__GT_pr_sr - 10 * Math.Log10(k * f1_13NA()) - f2_13();
        }

        public Double f2_26 ()
        {
            return f2_24() + 10 * Math.Log10(f1_13NA());
        }

        public Double f2_27()  // CN0.пер.цзс.д
        {
            return f2_25() + 10 * Math.Log10(f1_13NA());
        }

        public Double f2_27NA()
        {
            return f2_26() - 10 * Math.Log10(Rb_per_czs * 1e6);
        }

        public Double f2_28()  // CT.пер.цзс
        {
            return f2_26 () + 10 * Math.Log10 (k);
        }

        public Double f2_28NA()  // EbN0.пер.цзс.д
        {
            return f2_27() - 10 * Math.Log10(Rb_per_czs * 1e6);
        }

        public Double f2_29 ()
        {
            return f2_27 () + 10 * Math.Log10 (k);
        }

        public Double f2_30NA()  // CICN.пер.цзс
        {
            return f2_24() + 12.2;
        }

        public Double f2_31NB()  // CICN.пер.цзс.д
        {
            return f2_25() + 12.2;
        }

        public Double f2_32NA()  // CICN0.пер.цзс
        {
            return f2_26() + 12.2;
        }

        public Double f2_33NA()
        {
            return f2_27() + 12.2;
        }

        public Double f2_36 ()
        {
            return r_L_atm_pr_czs + r_attenuation_due_to_rain;
        }

        public Double f2_38 ()
        {
            return f2_36 () + f1_27();
        }

        public Double f2_43 ()
        {
            return f1_30 () + f2_38 ();
        }

        public double f2_46NA()  // Ta0.пр.цзс
        {
            double t1 = -0.1 * (r_L_atm_pr_czs);
            return (1 - Math.Pow(10, t1)) * T_atm_pr_czs + Math.Pow(10, t1) * T_g_pr_czs;
        }

        // T.а.пр.цзс
        public double f2_46 ()
        {
            double laft = 1.0 / Math.Pow(10, L_aft_pr_czs / 10);
            double latm = 1.0 / Math.Pow(10, r_L_atm_pr_czs / 10);
            double x = f2_46NA() * laft;
            double y = (1 - laft) * T0_pr_czs;
            double z = laft * (1 - latm) * (T_atm_pr_czs - T_s_pr_czs);
            return x + y + z;
        }

        // Ta0.пр.цзс.д
        public Double f2_46NB()
        {
            double t1 = -0.1 * (r_L_atm_pr_czs + r_attenuation_due_to_rain);
            return (1 - Math.Pow(10, t1)) * T_atm_pr_czs + Math.Pow(10, t1) * T_g_pr_czs;
        }

        public Double f2_47()  // TΣ.пр.цзс
        {
            return f2_46() + T0_pr_czs * (Math.Pow(10, 0.1 * f1_33()) - 1);
        }

        // T.а.пр.цзс.д
        public Double f2_47NA()
        {
            double laft = 1.0 / Math.Pow(10, L_aft_pr_czs / 10);
            double t1 = 1.0 / Math.Pow(10, r_L_atm_pr_czs / 10);
            return laft * f2_46NB() +
                (1 - laft) * T0_pr_czs +
                laft * (1 - t1) * (T_atm_pr_czs - T_s_pr_czs);
        }

        public Double f2_48() {
            return 10 * Math.Log10(k * f2_47() * f1_36NA());
        }

        public Double f2_48NA() // TΣ.пр.цзс.д
        {
            return f2_47NA() + T0_pr_czs * (Math.Pow(10, 0.1 * f1_33()) - 1);
        }

        public Double f2_48NB()
        {
            return 10 * Math.Log10(f2_48NA() / f2_47());
        }


        public Double f2_48NC()  // DND.тв
        {
            return r_attenuation_due_to_rain + f2_48NB();
        }

        public Double f2_48ND()  // N.пр.цзс.д
        {
            return 10 * Math.Log10(k * f2_48NA() * f1_36NA());
        }

        public Double f2_49()  // N0.пр.цзс
        {
            return 10 * Math.Log10(k * f2_47 () );
        }

        public Double f2_49NA()  // N0.пр.цзс.д
        {
            return 10 * Math.Log10(k * f2_48NA());
        }

        public Double f2_52 () {
            return equivalent_isotropic_radiated_power - f1_27() + f1_25 () - f1_30 ();
        }

        public Double f2_53 ()
        {
            return equivalent_isotropic_radiated_power - f2_38() + f1_25 () - f1_30 ();
        }

        public Double f2_54()
        {
            return equivalent_isotropic_radiated_power - 20 * Math.Log10(f1_21() * 1000) - 10 * Math.Log10(4 * PI);
        }

        public Double f2_56 ()
        {
            return f2_53 () - 20 * Math.Log10 (f1_21 ()) + 74.8;
        }

        public Double f2_57() {
            return f1_25 () - 10 * Math.Log10 (f2_47());
        }

        public Double f2_57NA()  // C.пр.цзс
        {
            return equivalent_isotropic_radiated_power + f1_25() - f1_10();
        }

        public Double f2_57NB()
        {
            return equivalent_isotropic_radiated_power + f1_25() - f2_13();
        }

        public Double f2_57NC()  // GT.пр.цзс.д
        {
            return f1_25() - 10 * Math.Log10(f2_48NA());
        }

        public Double f2_58()  // CN.пр.цзс
        {
            return equivalent_isotropic_radiated_power + 
                f2_57 () - 
                10 * Math.Log10(f1_13NA() * k) - 
                f1_31 ();
        }
        public Double f2_59 ()
        {
            return equivalent_isotropic_radiated_power + f2_57NC() - 10 * Math.Log10(f1_13NA() * k) - f2_43();
        }

        public Double f2_60 (){
            return equivalent_isotropic_radiated_power + f2_57 () - 10 * Math.Log10 (k) - f1_31 ();
        }

        public Double f2_61()  // CN0.пр.цзс.д
        {
            return equivalent_isotropic_radiated_power + f2_57NC () - 10 * Math.Log10 (k) - f2_43 ();
        }

        public double f2_61NA()
        {
            return f2_60() - 10 * Math.Log10(Rb_pr_czs * 1e6);
        }

        public Double f2_61NB()   // EbN0.пр.цзс.д
        {
            return f2_61() - 10 * Math.Log10(Rb_pr_czs * 1e6);
        }

        public Double f2_62() {
            return f2_60 () + 10 * Math.Log10 (k);
        }

        public Double f2_63()  // CT.пр.цзсд
        {
            return f2_61 () + 10 * Math.Log10 (k);

        }

        public Double f2_64 ()
        {
            return 10 * Math.Log10 (1 / (1 / Math.Pow (10, (f2_24 () / 10f))
                                       + 1 / Math.Pow (10, (f2_58 () / 10f))));
        }

        public Double f2_64NA()  // CICN.пр.цзс
        {
            return f2_58() + 12.2;
        }

        public Double f2_64NB()  // CICN.пр.цзс.д
        {
            return f2_59() + 12.2;
        }

        public Double f2_64NC()  // CICN0.пр.цзс.д
        {
            return f2_60() + 12.2;
        }

        public Double f2_64ND()  // CICN0.пр.цзс.д
        {
            return f2_61() + 12.2;
        }

        public Double f2_65 ()
        {
            return 10 * Math.Log10 (1 / (1 / Math.Pow (10, (f2_25 () / 10f))
                                       + 1 / Math.Pow (10, (f2_59 () / 10f))));
        }

        public Double f2_66 ()
        {
            return 10 * Math.Log10 (1 / (1 / Math.Pow (10, (f2_26 () / 10f))
                                         + 1 / Math.Pow (10, (f2_60 () / 10f))));
        }

        public Double f2_67 ()
        {
            return 10 * Math.Log10 (1 / (1 / Math.Pow (10, (f2_27 () / 10f))
                                         + 1 / Math.Pow (10, (f2_61 () / 10f))));
        }

        public Double f2_67NA()  // MΣ.CN0
        {
            return f2_66() - f2_67();
        }

        public double f2_68NA()  // EbN0Σ
        {
            return -10 * Math.Log10(Math.Pow(10, -0.1 * f2_27NA()) + Math.Pow(10, -0.1 * f2_61NA()));
        }

        public double f2_69NA()  // EbN0Σ.д
        {
            return -10 * Math.Log10(Math.Pow(10, -0.1 * f2_28NA()) + Math.Pow(10, -0.1 * f2_61NB()));
        }

        public Double f2_76()  // ΔCN.пр.цзс
        {
            return 10 * Math.Log10(1 + f1_50() / f2_47());
        }

        public Double f2_77()  // ΔCN.пр.цзс.д
        {
            return 10 * Math.Log10(1 + f1_50() / f2_48NA());
        }

        public double f2_79()
        {
            return equivalent_isotropic_radiated_power + f2_57NC() - f2_61NB() -
                   10 * Math.Log10(Rb_pr_czs * 1e6) - 10 * Math.Log10(k) - f1_27();
        }

        public double f2_81()
        {
            return erfc(Math.Sqrt(f2_61NA()));
        }

        public double f2_82()
        {
            return erfc(Math.Sqrt(f2_61NB()));
        }



        public double f3_9()  // L.ор.тв
        {
            return 12 * Math.Pow(f1_28() / f1_24(), 2);
        }

        public double f3_14()  // σ.атф.тв
        {
            return Math.Pow(10, -0.1 * L_aft_pr_czs);
        }

        public double f3_15()  // Sпр.тв
        {
            double d = 0.5 * r_antenna_diameter;
            return 10 * Math.Log10(PI * d * d * utilization_antenna_surface);
        }

        public double f3_16()  // Т.мшу.тв
        {
            return 290 * (Math.Pow(10, 0.1 * F_sh_pr_tv) - 1);
        }

        public double f3_21()  // TΣ.тв
        {
            return f3_16() + (1 - f3_14()) * T_s_tv + f3_14() * f2_46();
        }

        public double f3_22()
        {
            return f3_16() + (1 - f3_14()) * T_s_tv + f3_14() * f2_47NA();
        }

        public double f3_25()  // GT.тв
        {
            return 10 * Math.Log10(Math.Pow(10, 0.1 * (f1_25() - alpha_tv)) / f3_21());
        }

        public double f3_26()  // GT.тв.д
        {
            return 10 * Math.Log10(Math.Pow(10, 0.1 * (f1_25() - alpha_tv - f3_9())) / f3_22());
        }

        public double f3_27()  // CN.тв
        {
            return equivalent_isotropic_radiated_power + f3_25() - 10 * Math.Log10(k * f1_36NA()) - f1_31();
        }

        public double f3_28()  // CN.тв.д
        {
            return equivalent_isotropic_radiated_power + f3_26() - 10 * Math.Log10(k * f1_36NA()) - f2_43();
        }

        public double f3_29()  // EbN0.тв
        {
            return f3_27() + 10 * Math.Log10(1 / (Rb_pr_czs * 1e6)) + 10 * Math.Log10(f1_36NA());
        }

        public double f3_30()  // EbN0.тв.д
        {
            return f3_28() + 10 * Math.Log10(1 / (Rb_pr_czs * 1e6)) + 10 * Math.Log10(f1_36NA());
        }

        public double f3_31()  // L.s.тв
        {
            double D = f1_21() * 1e3;
            return 10 * Math.Log10(4 * PI * D * D);
        }

        public double f3_32()  // С.тв
        {
            return equivalent_isotropic_radiated_power -
                f3_31() + f3_15() + f1_25() - alpha_tv - f1_30();
        }

        public double f3_33()  // С.тв.д
        {
            return equivalent_isotropic_radiated_power -
                f3_31() + f3_15() + f1_25() - alpha_tv - f2_36() - f1_30();
        }

        public double f3_34()  // V.тв
        {
            return Math.Sqrt(Math.Pow(10, 0.1 * f3_32()) * R1);
        }

        public double f3_35()  // V.тв
        {
            return Math.Sqrt(Math.Pow(10, 0.1 * f3_33()) * R1);
        }

        public double f3_36()  // UV.тв
        {
            return 20 * Math.Log10(f3_34() * 1e6);
        }

        public double f3_37()  // UV.тв.д
        {
            return 20 * Math.Log10(f3_35() * 1e6);
        }

        public static Double parseDouble(TextBox tb, Label label, Double min = 0.0, Double max = 0.0, Boolean check = false)
        {
            Double result = 0.0;
            String text = tb.Text.ToString();
            text = text.Replace('.', ',');
            try
            {
                result = Double.Parse(text);
            }
            catch (Exception e)
            {
                throw new InvalidInputDataException(label.Text.ToString());
            }

            if (check)
            {

                Boolean res = min <= result && result <= max;

                if (!res)
                {
                    throw new InvalidInputDataException(label.Text.ToString() + " в пределах (" + min.ToString() +  ", "+ max.ToString() +")");
                }

            }     
           
            return result;
        }
    }

    [Serializable()]
    public class InvalidInputDataException : System.Exception
    {
        public InvalidInputDataException() : base() { }
        public InvalidInputDataException(string message) : base(message) { }
        public InvalidInputDataException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client. 
        protected InvalidInputDataException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}

