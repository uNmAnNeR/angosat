﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Angosat
{
    public partial class Form5 : Form
    {
        public TabPage tab;
        public Boolean isC;

        public Form5(Boolean isC, TabPage tabPage)
        {
            this.isC = isC;
            tab = tabPage;
            InitializeComponent();
            LoadData();
        }

        private void LoadData()
        {
            Evaluations evals = Evaluations.getEvals(tab);
            if (isC)
                this.T_total_pr_sr_input.Text = evals.C_BAND__T_total_pr_sr.ToString();
            else
                this.T_total_pr_sr_input.Text = evals.KU_BAND__T_total_pr_sr.ToString();
        }

        private void Form5_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Evaluations evals = Evaluations.getEvals(tab);

                if (isC)
                    evals.C_BAND__T_total_pr_sr = Evaluations.parseDouble(this.T_total_pr_sr_input, this.T_total_pr_sr_label);
                else
                    evals.KU_BAND__T_total_pr_sr = Evaluations.parseDouble(this.T_total_pr_sr_input, this.T_total_pr_sr_label);

            }
            catch (Exception ex)
            {
                MessageBox.Show("У вас ошибка в поле " + ex.Message,
                    "Исключительная ситуация",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button1);
                e.Cancel = true;
            }
        }
    }
}
