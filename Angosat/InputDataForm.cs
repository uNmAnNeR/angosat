﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace Angosat
{
    public class InputDataForm
    {
        private System.Windows.Forms.TabPage tabPage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;

        public System.Windows.Forms.TabPage createTabPage(Form1 that)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            tabPage = new System.Windows.Forms.TabPage("Новый");


            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();

            // 
            // tabPage1
            // 
            this.tabPage.Controls.Add(this.pictureBox1);
            this.tabPage.Controls.Add(this.button4);
            this.tabPage.Controls.Add(this.button3);
            this.tabPage.Controls.Add(this.button2);
            this.tabPage.Controls.Add(this.button1);
            this.tabPage.Location = new System.Drawing.Point(4, 22);
            this.tabPage.Name = "tabPage1";
            this.tabPage.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage.Size = new System.Drawing.Size(608, 380);
            this.tabPage.TabIndex = 2;
            this.tabPage.Text = "Новый";
            this.tabPage.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(8, 35);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(590, 283);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(157, 6);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(143, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "КА приемника АФУ";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(that.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(306, 6);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(143, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "КА передатчика АФУ";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(that.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(455, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(143, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "ЗС приемника АФУ";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(that.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(143, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "ЗС передатчика  АФУ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(that.button1_Click);

            return tabPage;
        }
    }
}
