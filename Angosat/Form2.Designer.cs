﻿namespace Angosat
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_atmospheric_loss = new System.Windows.Forms.Panel();
            this.input_atmospheric_loss = new System.Windows.Forms.TextBox();
            this.label_atmospheric_loss = new System.Windows.Forms.Label();
            this.panel_attenuation_due_to_rain = new System.Windows.Forms.Panel();
            this.input_attenuation_due_to_rain = new System.Windows.Forms.TextBox();
            this.label_attenuation_due_to_rain = new System.Windows.Forms.Label();
            this.panel_antenna_angle = new System.Windows.Forms.Panel();
            this.input_antenna_angle = new System.Windows.Forms.TextBox();
            this.label_antenna_angle = new System.Windows.Forms.Label();
            this.panel_operating_frequency = new System.Windows.Forms.Panel();
            this.input_operating_frequency = new System.Windows.Forms.TextBox();
            this.label_operating_frequency = new System.Windows.Forms.Label();
            this.panel_antenna_diameter = new System.Windows.Forms.Panel();
            this.input_antenna_diameter = new System.Windows.Forms.TextBox();
            this.label_antenna_diameter = new System.Windows.Forms.Label();
            this.panel_satellite_longitude = new System.Windows.Forms.Panel();
            this.input_station_longitude = new System.Windows.Forms.TextBox();
            this.label_satellite_longitude = new System.Windows.Forms.Label();
            this.panel_station_longitude = new System.Windows.Forms.Panel();
            this.input_satellite_longitude = new System.Windows.Forms.TextBox();
            this.label_station_longitude = new System.Windows.Forms.Label();
            this.panel_station_latitude = new System.Windows.Forms.Panel();
            this.input_station_latitude = new System.Windows.Forms.TextBox();
            this.label_station_latitude = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.n_per_czs_input = new System.Windows.Forms.TextBox();
            this.n_per_czs_label = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.fec_input = new System.Windows.Forms.TextBox();
            this.fec_label = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Rb_per_czs_input = new System.Windows.Forms.TextBox();
            this.Rb_per_czs_label = new System.Windows.Forms.Label();
            this.panel_atmospheric_loss.SuspendLayout();
            this.panel_attenuation_due_to_rain.SuspendLayout();
            this.panel_antenna_angle.SuspendLayout();
            this.panel_operating_frequency.SuspendLayout();
            this.panel_antenna_diameter.SuspendLayout();
            this.panel_satellite_longitude.SuspendLayout();
            this.panel_station_longitude.SuspendLayout();
            this.panel_station_latitude.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_atmospheric_loss
            // 
            this.panel_atmospheric_loss.AutoSize = true;
            this.panel_atmospheric_loss.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_atmospheric_loss.Controls.Add(this.input_atmospheric_loss);
            this.panel_atmospheric_loss.Controls.Add(this.label_atmospheric_loss);
            this.panel_atmospheric_loss.Location = new System.Drawing.Point(4, 200);
            this.panel_atmospheric_loss.Margin = new System.Windows.Forms.Padding(4);
            this.panel_atmospheric_loss.Name = "panel_atmospheric_loss";
            this.panel_atmospheric_loss.Size = new System.Drawing.Size(454, 20);
            this.panel_atmospheric_loss.TabIndex = 8;
            // 
            // input_atmospheric_loss
            // 
            this.input_atmospheric_loss.Location = new System.Drawing.Point(306, 0);
            this.input_atmospheric_loss.Margin = new System.Windows.Forms.Padding(0);
            this.input_atmospheric_loss.Name = "input_atmospheric_loss";
            this.input_atmospheric_loss.Size = new System.Drawing.Size(148, 20);
            this.input_atmospheric_loss.TabIndex = 34;
            this.input_atmospheric_loss.Text = "0.13";
            // 
            // label_atmospheric_loss
            // 
            this.label_atmospheric_loss.AutoSize = true;
            this.label_atmospheric_loss.Location = new System.Drawing.Point(0, 0);
            this.label_atmospheric_loss.Margin = new System.Windows.Forms.Padding(2);
            this.label_atmospheric_loss.Name = "label_atmospheric_loss";
            this.label_atmospheric_loss.Size = new System.Drawing.Size(132, 13);
            this.label_atmospheric_loss.TabIndex = 33;
            this.label_atmospheric_loss.Text = "Потери в атмосфере, дБ";
            // 
            // panel_attenuation_due_to_rain
            // 
            this.panel_attenuation_due_to_rain.AutoSize = true;
            this.panel_attenuation_due_to_rain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_attenuation_due_to_rain.Controls.Add(this.input_attenuation_due_to_rain);
            this.panel_attenuation_due_to_rain.Controls.Add(this.label_attenuation_due_to_rain);
            this.panel_attenuation_due_to_rain.Location = new System.Drawing.Point(4, 172);
            this.panel_attenuation_due_to_rain.Margin = new System.Windows.Forms.Padding(4);
            this.panel_attenuation_due_to_rain.Name = "panel_attenuation_due_to_rain";
            this.panel_attenuation_due_to_rain.Size = new System.Drawing.Size(454, 20);
            this.panel_attenuation_due_to_rain.TabIndex = 7;
            // 
            // input_attenuation_due_to_rain
            // 
            this.input_attenuation_due_to_rain.Location = new System.Drawing.Point(306, 0);
            this.input_attenuation_due_to_rain.Margin = new System.Windows.Forms.Padding(0);
            this.input_attenuation_due_to_rain.Name = "input_attenuation_due_to_rain";
            this.input_attenuation_due_to_rain.Size = new System.Drawing.Size(148, 20);
            this.input_attenuation_due_to_rain.TabIndex = 32;
            this.input_attenuation_due_to_rain.Text = "12,88";
            // 
            // label_attenuation_due_to_rain
            // 
            this.label_attenuation_due_to_rain.AutoSize = true;
            this.label_attenuation_due_to_rain.Location = new System.Drawing.Point(0, 0);
            this.label_attenuation_due_to_rain.Margin = new System.Windows.Forms.Padding(2);
            this.label_attenuation_due_to_rain.Name = "label_attenuation_due_to_rain";
            this.label_attenuation_due_to_rain.Size = new System.Drawing.Size(143, 13);
            this.label_attenuation_due_to_rain.TabIndex = 31;
            this.label_attenuation_due_to_rain.Text = "Затухание из-за дождя, дБ";
            // 
            // panel_antenna_angle
            // 
            this.panel_antenna_angle.AutoSize = true;
            this.panel_antenna_angle.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_antenna_angle.Controls.Add(this.input_antenna_angle);
            this.panel_antenna_angle.Controls.Add(this.label_antenna_angle);
            this.panel_antenna_angle.Location = new System.Drawing.Point(4, 144);
            this.panel_antenna_angle.Margin = new System.Windows.Forms.Padding(4);
            this.panel_antenna_angle.Name = "panel_antenna_angle";
            this.panel_antenna_angle.Size = new System.Drawing.Size(454, 20);
            this.panel_antenna_angle.TabIndex = 5;
            // 
            // input_antenna_angle
            // 
            this.input_antenna_angle.Location = new System.Drawing.Point(306, 0);
            this.input_antenna_angle.Margin = new System.Windows.Forms.Padding(0);
            this.input_antenna_angle.Name = "input_antenna_angle";
            this.input_antenna_angle.Size = new System.Drawing.Size(148, 20);
            this.input_antenna_angle.TabIndex = 29;
            this.input_antenna_angle.Text = "7.8";
            // 
            // label_antenna_angle
            // 
            this.label_antenna_angle.AutoSize = true;
            this.label_antenna_angle.Location = new System.Drawing.Point(0, 0);
            this.label_antenna_angle.Margin = new System.Windows.Forms.Padding(2);
            this.label_antenna_angle.Name = "label_antenna_angle";
            this.label_antenna_angle.Size = new System.Drawing.Size(180, 13);
            this.label_antenna_angle.TabIndex = 22;
            this.label_antenna_angle.Text = "Коэффициент наведения антенны";
            // 
            // panel_operating_frequency
            // 
            this.panel_operating_frequency.AutoSize = true;
            this.panel_operating_frequency.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_operating_frequency.Controls.Add(this.input_operating_frequency);
            this.panel_operating_frequency.Controls.Add(this.label_operating_frequency);
            this.panel_operating_frequency.Location = new System.Drawing.Point(4, 116);
            this.panel_operating_frequency.Margin = new System.Windows.Forms.Padding(4);
            this.panel_operating_frequency.Name = "panel_operating_frequency";
            this.panel_operating_frequency.Size = new System.Drawing.Size(454, 20);
            this.panel_operating_frequency.TabIndex = 4;
            // 
            // input_operating_frequency
            // 
            this.input_operating_frequency.Location = new System.Drawing.Point(306, 0);
            this.input_operating_frequency.Margin = new System.Windows.Forms.Padding(0);
            this.input_operating_frequency.Name = "input_operating_frequency";
            this.input_operating_frequency.Size = new System.Drawing.Size(148, 20);
            this.input_operating_frequency.TabIndex = 28;
            this.input_operating_frequency.Text = "14210000000";
            // 
            // label_operating_frequency
            // 
            this.label_operating_frequency.AutoSize = true;
            this.label_operating_frequency.Location = new System.Drawing.Point(0, 0);
            this.label_operating_frequency.Margin = new System.Windows.Forms.Padding(2);
            this.label_operating_frequency.Name = "label_operating_frequency";
            this.label_operating_frequency.Size = new System.Drawing.Size(118, 13);
            this.label_operating_frequency.TabIndex = 21;
            this.label_operating_frequency.Text = "Рабочая частота, МГц";
            // 
            // panel_antenna_diameter
            // 
            this.panel_antenna_diameter.AutoSize = true;
            this.panel_antenna_diameter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_antenna_diameter.Controls.Add(this.input_antenna_diameter);
            this.panel_antenna_diameter.Controls.Add(this.label_antenna_diameter);
            this.panel_antenna_diameter.Location = new System.Drawing.Point(4, 88);
            this.panel_antenna_diameter.Margin = new System.Windows.Forms.Padding(4);
            this.panel_antenna_diameter.Name = "panel_antenna_diameter";
            this.panel_antenna_diameter.Size = new System.Drawing.Size(454, 20);
            this.panel_antenna_diameter.TabIndex = 3;
            // 
            // input_antenna_diameter
            // 
            this.input_antenna_diameter.Location = new System.Drawing.Point(306, 0);
            this.input_antenna_diameter.Margin = new System.Windows.Forms.Padding(0);
            this.input_antenna_diameter.Name = "input_antenna_diameter";
            this.input_antenna_diameter.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.input_antenna_diameter.Size = new System.Drawing.Size(148, 20);
            this.input_antenna_diameter.TabIndex = 27;
            this.input_antenna_diameter.Tag = "";
            this.input_antenna_diameter.Text = "7.3";
            // 
            // label_antenna_diameter
            // 
            this.label_antenna_diameter.AutoSize = true;
            this.label_antenna_diameter.Location = new System.Drawing.Point(0, 0);
            this.label_antenna_diameter.Margin = new System.Windows.Forms.Padding(2);
            this.label_antenna_diameter.Name = "label_antenna_diameter";
            this.label_antenna_diameter.Size = new System.Drawing.Size(113, 13);
            this.label_antenna_diameter.TabIndex = 20;
            this.label_antenna_diameter.Text = "Диаметр антенны, м";
            // 
            // panel_satellite_longitude
            // 
            this.panel_satellite_longitude.AutoSize = true;
            this.panel_satellite_longitude.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_satellite_longitude.Controls.Add(this.input_station_longitude);
            this.panel_satellite_longitude.Controls.Add(this.label_satellite_longitude);
            this.panel_satellite_longitude.Location = new System.Drawing.Point(4, 60);
            this.panel_satellite_longitude.Margin = new System.Windows.Forms.Padding(4);
            this.panel_satellite_longitude.Name = "panel_satellite_longitude";
            this.panel_satellite_longitude.Size = new System.Drawing.Size(454, 20);
            this.panel_satellite_longitude.TabIndex = 2;
            // 
            // input_station_longitude
            // 
            this.input_station_longitude.Location = new System.Drawing.Point(306, 0);
            this.input_station_longitude.Margin = new System.Windows.Forms.Padding(0);
            this.input_station_longitude.Name = "input_station_longitude";
            this.input_station_longitude.Size = new System.Drawing.Size(148, 20);
            this.input_station_longitude.TabIndex = 26;
            this.input_station_longitude.Text = "13.33";
            // 
            // label_satellite_longitude
            // 
            this.label_satellite_longitude.AutoSize = true;
            this.label_satellite_longitude.Location = new System.Drawing.Point(0, 0);
            this.label_satellite_longitude.Margin = new System.Windows.Forms.Padding(2);
            this.label_satellite_longitude.Name = "label_satellite_longitude";
            this.label_satellite_longitude.Size = new System.Drawing.Size(165, 13);
            this.label_satellite_longitude.TabIndex = 19;
            this.label_satellite_longitude.Text = "Долгота земной станции, Град";
            // 
            // panel_station_longitude
            // 
            this.panel_station_longitude.AutoSize = true;
            this.panel_station_longitude.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_station_longitude.Controls.Add(this.input_satellite_longitude);
            this.panel_station_longitude.Controls.Add(this.label_station_longitude);
            this.panel_station_longitude.Location = new System.Drawing.Point(4, 32);
            this.panel_station_longitude.Margin = new System.Windows.Forms.Padding(4);
            this.panel_station_longitude.Name = "panel_station_longitude";
            this.panel_station_longitude.Size = new System.Drawing.Size(454, 20);
            this.panel_station_longitude.TabIndex = 1;
            // 
            // input_satellite_longitude
            // 
            this.input_satellite_longitude.Location = new System.Drawing.Point(306, 0);
            this.input_satellite_longitude.Margin = new System.Windows.Forms.Padding(0);
            this.input_satellite_longitude.Name = "input_satellite_longitude";
            this.input_satellite_longitude.Size = new System.Drawing.Size(148, 20);
            this.input_satellite_longitude.TabIndex = 25;
            this.input_satellite_longitude.Text = "14.5";
            // 
            // label_station_longitude
            // 
            this.label_station_longitude.AutoSize = true;
            this.label_station_longitude.Location = new System.Drawing.Point(0, 0);
            this.label_station_longitude.Margin = new System.Windows.Forms.Padding(2);
            this.label_station_longitude.Name = "label_station_longitude";
            this.label_station_longitude.Size = new System.Drawing.Size(199, 13);
            this.label_station_longitude.TabIndex = 18;
            this.label_station_longitude.Text = "Долгота под спутниковой точки, Град";
            // 
            // panel_station_latitude
            // 
            this.panel_station_latitude.AutoSize = true;
            this.panel_station_latitude.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_station_latitude.Controls.Add(this.input_station_latitude);
            this.panel_station_latitude.Controls.Add(this.label_station_latitude);
            this.panel_station_latitude.Location = new System.Drawing.Point(4, 4);
            this.panel_station_latitude.Margin = new System.Windows.Forms.Padding(4);
            this.panel_station_latitude.Name = "panel_station_latitude";
            this.panel_station_latitude.Size = new System.Drawing.Size(454, 20);
            this.panel_station_latitude.TabIndex = 0;
            // 
            // input_station_latitude
            // 
            this.input_station_latitude.Location = new System.Drawing.Point(306, 0);
            this.input_station_latitude.Margin = new System.Windows.Forms.Padding(0);
            this.input_station_latitude.Name = "input_station_latitude";
            this.input_station_latitude.Size = new System.Drawing.Size(148, 20);
            this.input_station_latitude.TabIndex = 23;
            this.input_station_latitude.Text = "8.5";
            // 
            // label_station_latitude
            // 
            this.label_station_latitude.AutoSize = true;
            this.label_station_latitude.Location = new System.Drawing.Point(0, 0);
            this.label_station_latitude.Margin = new System.Windows.Forms.Padding(2);
            this.label_station_latitude.Name = "label_station_latitude";
            this.label_station_latitude.Size = new System.Drawing.Size(172, 13);
            this.label_station_latitude.TabIndex = 17;
            this.label_station_latitude.Text = "Широта наземной станции, Град";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.panel_station_latitude);
            this.flowLayoutPanel1.Controls.Add(this.panel_station_longitude);
            this.flowLayoutPanel1.Controls.Add(this.panel_satellite_longitude);
            this.flowLayoutPanel1.Controls.Add(this.panel_antenna_diameter);
            this.flowLayoutPanel1.Controls.Add(this.panel_operating_frequency);
            this.flowLayoutPanel1.Controls.Add(this.panel_antenna_angle);
            this.flowLayoutPanel1.Controls.Add(this.panel_attenuation_due_to_rain);
            this.flowLayoutPanel1.Controls.Add(this.panel_atmospheric_loss);
            this.flowLayoutPanel1.Controls.Add(this.panel1);
            this.flowLayoutPanel1.Controls.Add(this.panel2);
            this.flowLayoutPanel1.Controls.Add(this.panel3);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(9, 9);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0, 0, 8, 8);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(462, 308);
            this.flowLayoutPanel1.TabIndex = 51;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.n_per_czs_input);
            this.panel1.Controls.Add(this.n_per_czs_label);
            this.panel1.Location = new System.Drawing.Point(4, 228);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(454, 20);
            this.panel1.TabIndex = 9;
            // 
            // n_per_czs_input
            // 
            this.n_per_czs_input.Location = new System.Drawing.Point(306, 0);
            this.n_per_czs_input.Margin = new System.Windows.Forms.Padding(0);
            this.n_per_czs_input.Name = "n_per_czs_input";
            this.n_per_czs_input.Size = new System.Drawing.Size(148, 20);
            this.n_per_czs_input.TabIndex = 34;
            this.n_per_czs_input.Text = "2";
            // 
            // n_per_czs_label
            // 
            this.n_per_czs_label.AutoSize = true;
            this.n_per_czs_label.Location = new System.Drawing.Point(0, 0);
            this.n_per_czs_label.Margin = new System.Windows.Forms.Padding(2);
            this.n_per_czs_label.Name = "n_per_czs_label";
            this.n_per_czs_label.Size = new System.Drawing.Size(169, 13);
            this.n_per_czs_label.TabIndex = 33;
            this.n_per_czs_label.Text = "Количество бит на один символ";
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel2.Controls.Add(this.fec_input);
            this.panel2.Controls.Add(this.fec_label);
            this.panel2.Location = new System.Drawing.Point(4, 256);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(454, 20);
            this.panel2.TabIndex = 10;
            // 
            // fec_input
            // 
            this.fec_input.Location = new System.Drawing.Point(306, 0);
            this.fec_input.Margin = new System.Windows.Forms.Padding(0);
            this.fec_input.Name = "fec_input";
            this.fec_input.Size = new System.Drawing.Size(148, 20);
            this.fec_input.TabIndex = 34;
            this.fec_input.Text = "0.875";
            // 
            // fec_label
            // 
            this.fec_label.AutoSize = true;
            this.fec_label.Location = new System.Drawing.Point(0, 0);
            this.fec_label.Margin = new System.Windows.Forms.Padding(2);
            this.fec_label.Name = "fec_label";
            this.fec_label.Size = new System.Drawing.Size(27, 13);
            this.fec_label.TabIndex = 33;
            this.fec_label.Text = "FEC";
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel3.Controls.Add(this.Rb_per_czs_input);
            this.panel3.Controls.Add(this.Rb_per_czs_label);
            this.panel3.Location = new System.Drawing.Point(4, 284);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(454, 20);
            this.panel3.TabIndex = 11;
            // 
            // Rb_per_czs_input
            // 
            this.Rb_per_czs_input.Location = new System.Drawing.Point(306, 0);
            this.Rb_per_czs_input.Margin = new System.Windows.Forms.Padding(0);
            this.Rb_per_czs_input.Name = "Rb_per_czs_input";
            this.Rb_per_czs_input.Size = new System.Drawing.Size(148, 20);
            this.Rb_per_czs_input.TabIndex = 34;
            this.Rb_per_czs_input.Text = "85";
            // 
            // Rb_per_czs_label
            // 
            this.Rb_per_czs_label.AutoSize = true;
            this.Rb_per_czs_label.Location = new System.Drawing.Point(0, 0);
            this.Rb_per_czs_label.Margin = new System.Windows.Forms.Padding(2);
            this.Rb_per_czs_label.Name = "Rb_per_czs_label";
            this.Rb_per_czs_label.Size = new System.Drawing.Size(158, 13);
            this.Rb_per_czs_label.TabIndex = 33;
            this.Rb_per_czs_label.Text = "Скорость передачи символов";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(586, 388);
            this.Controls.Add(this.flowLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form2";
            this.Text = "ЗС Передатчика АФУ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_FormClosing);
            this.panel_atmospheric_loss.ResumeLayout(false);
            this.panel_atmospheric_loss.PerformLayout();
            this.panel_attenuation_due_to_rain.ResumeLayout(false);
            this.panel_attenuation_due_to_rain.PerformLayout();
            this.panel_antenna_angle.ResumeLayout(false);
            this.panel_antenna_angle.PerformLayout();
            this.panel_operating_frequency.ResumeLayout(false);
            this.panel_operating_frequency.PerformLayout();
            this.panel_antenna_diameter.ResumeLayout(false);
            this.panel_antenna_diameter.PerformLayout();
            this.panel_satellite_longitude.ResumeLayout(false);
            this.panel_satellite_longitude.PerformLayout();
            this.panel_station_longitude.ResumeLayout(false);
            this.panel_station_longitude.PerformLayout();
            this.panel_station_latitude.ResumeLayout(false);
            this.panel_station_latitude.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel_atmospheric_loss;
        private System.Windows.Forms.TextBox input_atmospheric_loss;
        private System.Windows.Forms.Label label_atmospheric_loss;
        private System.Windows.Forms.Panel panel_attenuation_due_to_rain;
        private System.Windows.Forms.TextBox input_attenuation_due_to_rain;
        private System.Windows.Forms.Label label_attenuation_due_to_rain;
        private System.Windows.Forms.Panel panel_antenna_angle;
        private System.Windows.Forms.TextBox input_antenna_angle;
        private System.Windows.Forms.Label label_antenna_angle;
        private System.Windows.Forms.Panel panel_operating_frequency;
        private System.Windows.Forms.TextBox input_operating_frequency;
        private System.Windows.Forms.Label label_operating_frequency;
        private System.Windows.Forms.Panel panel_antenna_diameter;
        private System.Windows.Forms.TextBox input_antenna_diameter;
        private System.Windows.Forms.Label label_antenna_diameter;
        private System.Windows.Forms.Panel panel_satellite_longitude;
        private System.Windows.Forms.TextBox input_station_longitude;
        private System.Windows.Forms.Label label_satellite_longitude;
        private System.Windows.Forms.Panel panel_station_longitude;
        private System.Windows.Forms.TextBox input_satellite_longitude;
        private System.Windows.Forms.Label label_station_longitude;
        private System.Windows.Forms.Panel panel_station_latitude;
        private System.Windows.Forms.TextBox input_station_latitude;
        private System.Windows.Forms.Label label_station_latitude;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox n_per_czs_input;
        private System.Windows.Forms.Label n_per_czs_label;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox fec_input;
        private System.Windows.Forms.Label fec_label;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox Rb_per_czs_input;
        private System.Windows.Forms.Label Rb_per_czs_label;
    }
}