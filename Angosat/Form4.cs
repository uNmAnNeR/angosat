﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Angosat
{
    public partial class Form4 : Form
    {
        public TabPage tab;

        public Form4(TabPage tabPage)
        {
            tab = tabPage;
            InitializeComponent();
            LoadData();
        }

        private void LoadData()
        {
            Evaluations evals = Evaluations.getEvals(tab);
            this.input_equivalent_isotropic_radiated_power.Text = evals.equivalent_isotropic_radiated_power.ToString();
        }
    

        private void Form4_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Double equivalent_isotropic_radiated_power = Evaluations.parseDouble(this.input_equivalent_isotropic_radiated_power, this.label_equivalent_isotropic_radiated_power, 0, 0, false);
                Evaluations evals = Evaluations.getEvals(tab);
                evals.equivalent_isotropic_radiated_power = equivalent_isotropic_radiated_power;
            }
            catch (Exception ex)
            {
                MessageBox.Show("У вас ошибка в поле " + ex.Message,
                    "Исключительная ситуация",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button1);
                e.Cancel = true;
            }
        }
    }
}
