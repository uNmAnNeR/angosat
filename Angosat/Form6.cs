﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Angosat
{
    public partial class Form6 : Form
    {
        private System.ComponentModel.IContainer components = null;

        FlowLayoutPanel flowLayoutPanel1;

        public enum Band { C, Ku, KuTV };
        Band currentBand;
        
        TabPage page;
        List<Row> rows;

        public class Row
        {
            public String label;
            public String value1;
            public String value2;

            public Row(String l, String v1, String v2){
                label = l;
                value1 = v1;
                value2 = v2;
            }

            override public String ToString()
            {
                return String.Format("{0}, {1}, {2}", label, value1, value2);
            }
        }




        public static List<Row> evalDataC(TabPage page)
        {
            List<Row> rows = new List<Row>();
            Evaluations ev = Evaluations.getEvals(page);
            rows.Add(new Row("ПАРАМЕТР",                                                                            "ЗС ВВЕРХ",                    "ЗС ВНИЗ"));
            rows.Add(new Row("Расстояние ЦЗС- СР, СР - ЦЗС, км",                                                    ev.f1_1().ToString(),       ev.f1_21().ToString()));
            rows.Add(new Row("Угол между ЗС и спутником, Град",                                                     ev.f1_2().ToString(),       ev.f1_22().ToString()));
            rows.Add(new Row("Угол поляризации ЦЗС в плоскости перпендикулряной к плоскости экватора, Град",        ev.f1_3().ToString(),       ev.f1_23().ToString()));
            rows.Add(new Row("Ширина диаграммы направленности антенны, Град",                                       ev.f1_4().ToString(),       ev.f1_24().ToString()));
            rows.Add(new Row("Максимальный коэффциент усиления, дБ",                                                ev.f1_5().ToString(),       ev.f1_25().ToString()));
            rows.Add(new Row("Эффективная площадь покрытия ЗС, м² ",                                                "",                         ev.f1_26().ToString()));
            rows.Add(new Row("Затухание сигнала в свободном пространстве, дБ",                                      ev.f1_6().ToString(),       ev.f1_27().ToString()));
            rows.Add(new Row("Угол неточности наведения антенны, дБ",                                               ev.f1_7().ToString(),       ev.f1_28().ToString()));

            rows.Add(new Row("Потери из-за неточности наведения антенны, дБ",                                       ev.f1_8().ToString(),       ev.f1_29().ToString()));
            rows.Add(new Row("Суммарные дополнительные потери, дБ",                                                 ev.f1_9().ToString(),       ev.f1_30().ToString()));
            rows.Add(new Row("Максимальные потери, дБ",                                                             ev.f1_10().ToString(),      ev.f1_31().ToString()));
            rows.Add(new Row("Коэффициент усиления антенны, дБ",                                                    ev.f1_11().ToString(),      ev.f1_32().ToString()));
            rows.Add(new Row("Шумовая температура при ясном небе, К",                                               "",                         ev.f1_33NA().ToString()));

            rows.Add(new Row("Общий шум-фактор премника, дБ",                                                       "",                         ev.f1_33().ToString()));
            rows.Add(new Row("Эквивалентная шумовая температура антенны, К",                                        "",                         ev.f1_34().ToString()));
            rows.Add(new Row("Суммарная шумовая температура приемного тракта СР, К", ev.C_BAND__T_total_pr_sr.ToString(), ev.f1_35().ToString()));
            rows.Add(new Row("Максимальная мощность теплового шума, Вт",                                            ev.f1_13().ToString(),      ev.f1_36().ToString()));
            rows.Add(new Row("Спектральная плотность мощности шума, Вт/Гц",                                         ev.f1_14().ToString(),      ev.f1_37().ToString()));
            rows.Add(new Row("Потери выходной мощности на одной несущей, дБ",                                       ev.f1_15NA().ToString(),    ""));
            rows.Add(new Row("Эффективная мощность на ЗС, дБ·Вт",                                                   ev.f1_15().ToString(),      ev.f1_38().ToString()));
            rows.Add(new Row("Плотность потока мощности насыщения, дБ·Вт/м²",                                       ev.f1_16().ToString(),      ev.f1_39().ToString()));
            rows.Add(new Row("Напряженность электромагнитного поля Е, дБ·Вт",                                       ev.f1_17().ToString(),      ev.f1_40().ToString()));
            rows.Add(new Row("Мощность на несущей, дБ",                                                    ev.f1_17NA().ToString(),    ev.f1_41NA().ToString()));

            rows.Add(new Row("Добротность, дБ/К",                                                                   "",                         ev.f1_41().ToString()));
            rows.Add(new Row("Отношение сигнал-шум, дБ·Вт",                                                         ev.f1_18().ToString(),      ev.f1_42().ToString()));
            rows.Add(new Row("Отношение сигнал-шум относительно спектральной плотности мощности , дБ·Вт",           ev.f1_19().ToString(),      ev.f1_43().ToString()));
            rows.Add(new Row("Скорость передачи символов",                                                                   ev.R_c_per_czs().ToString(), ev.R_c_pr_czs().ToString()));
            rows.Add(new Row("Полоса пропускания ЛСС", ev.f1_13NA().ToString(), ev.f1_36NA().ToString()));
            rows.Add(new Row("Отношение энергии бит к спектральной мощности шума, дБ",                              ev.f1_20NA().ToString(),    ev.f1_43NA().ToString()));
            rows.Add(new Row("Отношение несущей и суммарной температуры, дБ·Гц",                                    ev.f1_20().ToString(),      ev.f1_44().ToString()));
            rows.Add(new Row("Суммарное отношение сигнал-шум при мощости теплового шума, дБ",                       "",                         ev.f1_45().ToString()));
            rows.Add(new Row("Суммарное отношение сигнал-шум при спектральной плотности мощности шума, дБ·Гц",      "",                         ev.f1_46().ToString()));
            rows.Add(new Row("Суммарное отношение энергия бита к спектральная плотность, дБ",                       "",                         ev.f1_46NA().ToString()));
            rows.Add(new Row("Радиояркостная температура в С диапазоне, К",                                         "",                         ev.f1_47().ToString()));
            rows.Add(new Row("Солнечная шумовая температура на ЗС, К",                                              "",                         ev.f1_49().ToString()));
            rows.Add(new Row("Увеличение солнечной шумовой температуры, К",                                         "",                         ev.f1_50().ToString()));
            rows.Add(new Row("Увеличение солнечной шумовой температуры при использование поляризатором, К",         "",                         ev.f1_51().ToString()));
            rows.Add(new Row("Количество дней влияния Солнца на ЗС, день",                                          "",                         ev.f1_52().ToString()));
            rows.Add(new Row("Максимальное время влияния Солнца на ЗС, мин",                                        "",                         ev.f1_53().ToString()));
            rows.Add(new Row("Общая продолжительность времени влияния Солнца на ЗС, мин",                           "",                         ev.f1_54().ToString()));
            rows.Add(new Row("Ухудшение отношения сигнал-шум из-за Солнечной температуры, дБ",                      "",                         ev.f1_55().ToString()));
            rows.Add(new Row("Спектральная эффективность", ev.f1_21NA().ToString(), ev.f1_57().ToString()));
            rows.Add(new Row("Энергетический запас линии связи", "", ev.f1_56().ToString()));
            rows.Add(new Row("Вероятность битовой ошибки", "", ev.f1_58().ToString()));

            return rows;
        }

        public static List<Row> evalDataKu(TabPage page)
        {
            List<Row> rows = new List<Row>();
            Evaluations ev = Evaluations.getEvals(page);
            rows.Add(new Row("ПАРАМЕТР",                                                                            "ЗС ВВЕРХ",                    "ЗС ВНИЗ"));
            rows.Add(new Row("Расстояние ЦЗС - СР, СР - ЦЗС, км",                                                   ev.f1_1().ToString(),       ev.f1_21().ToString()));
            rows.Add(new Row("Угол между ЗС и спутником, Град",                                                     ev.f1_2().ToString(),       ev.f1_22().ToString()));
            rows.Add(new Row("Угол поляризации ЦЗС в плоскости перпендикулряной к плоскости экватора, Град",        ev.f1_3().ToString(),       ev.f1_23().ToString()));
            rows.Add(new Row("Ширина диаграммы направленности антенны, Град",                                       ev.f1_4().ToString(),       ev.f1_24().ToString()));
            rows.Add(new Row("Максимальный коэффциент усиления, дБ",                                                ev.f1_5().ToString(),       ev.f1_25().ToString()));

            rows.Add(new Row("Суммарный атмосферный вклад в виде затухания, дБ" ,                                   ev.f2_6().ToString(),       ev.f2_36().ToString()));

            rows.Add(new Row("Эффективная площадь покрытия ЗС, м²",                                                 "",                         ev.f1_26().ToString()));
            rows.Add(new Row("Затухание сигнала в свободном пространстве, дБ",                                      ev.f1_6().ToString(),       ev.f1_27().ToString()));
            rows.Add(new Row("при дожде, дБ",                                                                       ev.f2_8().ToString(),       ev.f2_38().ToString()));
            
            rows.Add(new Row("Угол неточности наведения антенны, дБ",                                               ev.f1_7().ToString(),       ev.f1_28().ToString()));
                                                                                                                        
            rows.Add(new Row("Потери из-за неточности наведения антенны, дБ",                                       ev.f1_8().ToString(),       ev.f1_29().ToString()));
            rows.Add(new Row("Суммарные дополнительные потери, дБ",                                                 ev.f1_9().ToString(),       ev.f1_30().ToString()));
            rows.Add(new Row("Максимальные потери, дБ",                                                             ev.f1_10().ToString(),      ev.f1_31().ToString()));
            rows.Add(new Row("при дожде, дБ",                                                                       ev.f2_13().ToString(),      ev.f2_43().ToString()));
            rows.Add(new Row("Коэффициент усиления антенны, дБ",                                                    ev.f1_11().ToString(),      ev.f1_32().ToString()));
                                                                                                                        
            rows.Add(new Row("Общий шум-фактор премника, дБ",                                                       "",                         ev.f1_33().ToString()));
            rows.Add(new Row("Шумовая температура при ясном небе, К",                                               "",                         ev.f1_33NA().ToString()));
            rows.Add(new Row("Шумовая температура при дожде, К",                                                    "",                         ev.f2_46NB().ToString()));
            rows.Add(new Row("Эквивалентная шумовая температура антенны при ясном небе, К",                         "",                         ev.f2_46().ToString()));
            rows.Add(new Row("Эквивалентная шумовая температура антенны при дожде, К",                              "",                         ev.f2_47NA().ToString()));

            rows.Add(new Row("Суммарная шумовая температура приемного тракта СР, К",                                ev.f2_15().ToString(),      ev.f2_47().ToString()));
            rows.Add(new Row("Суммарная шумовая температура приемного тракта при дожде, К",                         "",                         ev.f2_48NA().ToString()));
            rows.Add(new Row("Возрастание шумов",                                                                   "",                         ev.f2_48NB().ToString()));
            rows.Add(new Row("Снижение эффективности ЛСС",                                                          "",                         ev.f2_48NC().ToString()));
            rows.Add(new Row("Максимальная мощность теплового шума, дБ",                                            ev.f2_48().ToString(),      ev.f2_48ND().ToString()));
            rows.Add(new Row("Спектральная плотность мощности шума, Вт/Гц",                                         ev.f2_17().ToString(),      ev.f2_49().ToString()));
            rows.Add(new Row("Спектральная плотность мощности шума при дожде, Вт/Гц",                               "",                         ev.f2_49NA().ToString()));
                
            rows.Add(new Row("Эффективная мощность на ЗС, дБ·Вт",                                                   ev.f2_18NA().ToString(),      ev.f2_52().ToString()));
            rows.Add(new Row("при дожде",                                                                           "",    ev.f2_53().ToString()));


            rows.Add(new Row("Плотность потока мощности насыщения, дБ·Вт/м²",                                       ev.f2_20().ToString(),      ev.f2_54().ToString()));
            //rows.Add(new Row("при дожде",                                                                           ev.f2_21().ToString(),      ""));
            rows.Add(new Row("Напряженность электромагнитного поля Е, дБ·Вт",                                       ev.f2_22().ToString(),      ev.f1_40().ToString()));
            rows.Add(new Row("при дожде",                                                                           "",                         ev.f2_56().ToString()));

            rows.Add(new Row("Выходная мощность несущей ЗС", ev.f2_23NA().ToString(), ev.f2_57NA().ToString()));
            rows.Add(new Row("при дожде", ev.f2_24NB().ToString(), ev.f2_57NB().ToString()));

            rows.Add(new Row("Добротность, дБ/К",                                                                   "",                         ev.f2_57().ToString()));
            rows.Add(new Row("Добротность при дожде, дБ/К", "", ev.f2_57NC().ToString()));
            rows.Add(new Row("Отношение сигнал-шум , дБ·Вт",                                                        ev.f2_24().ToString(),      ev.f2_58().ToString()));
            rows.Add(new Row("при дожде",                                                                           ev.f2_25().ToString(),      ev.f2_59().ToString()));
            rows.Add(new Row("Отношение сигнал-шум относительно спектральной плотности мощности , дБ·Вт",           ev.f2_26().ToString(),      ""));
            rows.Add(new Row("при дожде",                                                                           ev.f2_27().ToString(),      ""));

            rows.Add(new Row("Отношение сигнал-шум относительно спектральной плотности мощности шума, дБ·Гц",       "",                         ev.f2_60().ToString()));
            rows.Add(new Row("при дожде, дБ·Гц",                                                                    "",                         ev.f2_61().ToString()));

            rows.Add(new Row("Скорость передачи символов", ev.R_c_per_czs().ToString(), ev.R_c_pr_czs().ToString()));
            rows.Add(new Row("Полоса пропускания ЛСС", ev.f1_13NA().ToString(), ev.f1_36NA().ToString()));
            rows.Add(new Row("Отношение энергии бит к спектральной мощности шума, дБ", ev.f2_27NA().ToString(), ev.f1_43NA().ToString()));
            rows.Add(new Row("Отношение энергии бит к спектральной мощности шума при дожде, дБ", ev.f2_28NA().ToString(), ev.f2_61NB().ToString()));

            rows.Add(new Row("Отношение несущей и суммарной температуры, дБ·Гц",                                    ev.f2_28().ToString(),      ev.f2_62().ToString()));
            rows.Add(new Row("при дожде",                                                                           ev.f2_29().ToString(),      ev.f2_63().ToString()));
            rows.Add(new Row("Допустимый критерий C/I", ev.f2_30NA().ToString(), ev.f2_64NA().ToString()));
            rows.Add(new Row("при дожде", ev.f2_31NB().ToString(), ev.f2_64NB().ToString()));
            rows.Add(new Row("Допустимый критерий C/I  при передачи цивровой сигнала", ev.f2_32NA().ToString(), ev.f2_64NC().ToString()));
            rows.Add(new Row("при дожде", ev.f2_33NA().ToString(), ev.f2_64ND().ToString()));
            rows.Add(new Row("Суммарное отношение сигнал-шум при мощности теплового шума, дБ",                      "",                         ev.f2_64().ToString()));
            rows.Add(new Row("при дожде",                                                                           "",                         ev.f2_65().ToString()));
            rows.Add(new Row("Суммарное отношение несущей к шум сигнала с тепловой шум при мощности теплового шума, дБ·Гц",      "",                         ev.f2_66().ToString()));
            rows.Add(new Row("при дожде",                                                                           "",                         ev.f2_67().ToString()));
            rows.Add(new Row("Суммарное отношение энергия бита к спектральная плотность мощности шума", "", ev.f2_68NA().ToString()));
            rows.Add(new Row("при дожде", "", ev.f2_69NA().ToString()));
            rows.Add(new Row("Суммарное энергетическое запас на все линии", "", ev.f2_67NA().ToString())); // TODO ne po russki!

            rows.Add(new Row("Радиояркостная температура в С диапазоне, К",                                         "",                         ev.f1_47().ToString()));
            rows.Add(new Row("Солнечная шумовая температура на ЗС, К",                                              "",                         ev.f1_49().ToString()));
            rows.Add(new Row("Увеличение солнечной шумовой температуры, К",                                         "",                         ev.f1_50().ToString()));
            rows.Add(new Row("Увеличение солнечной шумовой температуры при использование поляризатором, К",         "",                         ev.f1_51().ToString()));
            rows.Add(new Row("Количество дней влияния Солнца на ЗС, день",                                          "",                         ev.f1_52().ToString()));
            rows.Add(new Row("Максимальное время влияния Солнца на ЗС, мин",                                        "",                         ev.f1_53().ToString()));
            rows.Add(new Row("Общая продолжительность времени влияния Солнца на ЗС, мин",                           "",                         ev.f1_54().ToString()));
            rows.Add(new Row("Ухудшение отношения сигнал-шум из-за Солнечной температуры, дБ",                      "",                         ev.f2_76().ToString()));
            rows.Add(new Row("при дожде", "", ev.f2_77().ToString()));

            rows.Add(new Row("Спектральная эффективность", ev.f1_21NA().ToString(), ev.f1_57().ToString()));
            rows.Add(new Row("Энергетический запас линии связи", "", ev.f1_56().ToString()));
            rows.Add(new Row("при дожде", "", ev.f2_79().ToString()));
            rows.Add(new Row("Вероятность битовой ошибки", "", ev.f2_81().ToString()));
            rows.Add(new Row("при дожде", "", ev.f2_82().ToString()));

            return rows;
        }

        public static List<Row> evalDataKuTV(TabPage page)
        {
            List<Row> rows = new List<Row>();
            Evaluations ev = Evaluations.getEvals(page);
            rows.Add(new Row("ПАРАМЕТР", "ЗС ВВЕРХ", "ЗС ВНИЗ"));

            rows.Add(new Row("Расстояние ЦЗС - СР, СР - ЦЗС, км", "", ev.f1_21().ToString()));
            rows.Add(new Row("Угол между ЗС и спутником, Град", "", ev.f1_22().ToString()));
            rows.Add(new Row("Ширина диаграммы направленности антенны, Град", "", ev.f1_24().ToString()));
            rows.Add(new Row("Максимальный коэффциент усиления, дБ", "", ev.f1_25().ToString()));
            rows.Add(new Row("Суммарный атмосферный вклад в виде затухания, дБ", "", ev.f2_36().ToString()));
            rows.Add(new Row("Эффективная площадь покрытия антенны приемника, м²", "", ev.f3_15().ToString()));
            rows.Add(new Row("Затухание сигнала в свободном пространстве, дБ", "", ev.f1_27().ToString()));
            rows.Add(new Row("при дожде, дБ", "", ev.f2_38().ToString()));
            rows.Add(new Row("Угол неточности наведения антенны, дБ", "", ev.f1_28().ToString()));
            rows.Add(new Row("Потери из-за неточности наведения антенны, дБ", "", ev.f1_29().ToString()));
            rows.Add(new Row("Суммарные дополнительные потери, дБ", "", ev.f1_30().ToString()));
            rows.Add(new Row("Максимальные потери, дБ", "", ev.f1_31().ToString()));
            rows.Add(new Row("при дожде, дБ", "", ev.f2_43().ToString()));
            rows.Add(new Row("Коэффициент усиления антенны, дБ", "", ev.f1_32().ToString()));
            rows.Add(new Row("Проницаемость среды", "", ev.f3_14().ToString()));
            rows.Add(new Row("Общий шум-фактор премника, дБ", "", ev.f1_33().ToString()));
            rows.Add(new Row("Шумовая температура при ясном небе, К", "", ev.f1_33NA().ToString()));
            rows.Add(new Row("при дожде, К", "", ev.f2_46NB().ToString()));
            rows.Add(new Row("Эквивалентная шумовая температура антенны при ясном небе, К", "", ev.f2_46().ToString()));
            rows.Add(new Row("при дожде, К", "", ev.f2_47NA().ToString()));
            rows.Add(new Row("Суммарная шумовая температура приемного тракта СР, К", "", ev.f3_21().ToString()));
            rows.Add(new Row("при дожде, К", "", ev.f3_22().ToString()));
            rows.Add(new Row("Возрастание шумов", "", ev.f2_48NB().ToString()));
            rows.Add(new Row("Снижение эффективности ЛСС", "", ev.f2_48NC().ToString()));
            rows.Add(new Row("Добротность, дБ", "", ev.f3_25().ToString()));
            rows.Add(new Row("при дожде, дБ", "", ev.f3_26().ToString()));
            rows.Add(new Row("Скорость передачи символов", "", ev.R_c_pr_czs().ToString()));
            rows.Add(new Row("Полоса пропускания ЛСС", "", ev.f1_36NA().ToString()));
            rows.Add(new Row("Отношение сигнал-шум , дБ·Вт", "", ev.f2_58().ToString()));
            rows.Add(new Row("при дожде", "", ev.f2_59().ToString()));
            rows.Add(new Row("Отношение энергии бит к спектральной мощности шума, дБ", "", ev.f3_29().ToString()));
            rows.Add(new Row("при дожде, дБ", "", ev.f3_30().ToString()));
            rows.Add(new Row("Расходимость луча, дБ", "", ev.f3_31().ToString()));
            rows.Add(new Row("Мощность сигнала несущей, дБ", "", ev.f3_32().ToString()));
            rows.Add(new Row("при дожде, дБ", "", ev.f3_33().ToString()));
            rows.Add(new Row("Уровень выходного сигнал LNB, дБмкВ", "", ev.f3_36().ToString()));
            rows.Add(new Row("при дожде, дБмкВ", "", ev.f3_37().ToString()));
            rows.Add(new Row("Спектральная эффективность", ev.f1_21NA().ToString(), ev.f1_57().ToString()));
            rows.Add(new Row("Энергетический запас линии связи", "", ev.f1_56().ToString()));
            rows.Add(new Row("при дожде", "", ev.f2_79().ToString()));


            return rows;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public Form6(Band band, TabPage page)
        {
            this.currentBand = band;
            this.page = page;

            flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            //flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            flowLayoutPanel1.AutoSize = true;
            flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(0); 
            flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            flowLayoutPanel1.Name = "flowLayoutPanel1";
            flowLayoutPanel1.AutoSize = true;
            flowLayoutPanel1.TabIndex = 0;

            this.AutoScroll = true;
            
            //AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Padding = new System.Windows.Forms.Padding(0); 
            Margin = new System.Windows.Forms.Padding(0);
            Width = 900;
            Height = 400;
            Name = "Form6";
            Text = "Результаты расчета";


            switch (this.currentBand) {
                case Band.C: rows = evalDataC(page); break;
                case Band.Ku: rows = evalDataKu(page); break;
                case Band.KuTV: rows = evalDataKuTV(page); break;
            }

            int i = 1;
            foreach (Row row in rows){
                flowLayoutPanel1.Controls.Add(BuildRow(row, i));
                i++;
            }
            
            Controls.Add(flowLayoutPanel1);
        }


        private Panel BuildRow(Row row, int i)
        {
            Panel panel;
            Label label1, label2, label3;

            Boolean header = row.label.Equals("ПАРАМЕТР");

            panel = new System.Windows.Forms.Panel();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();

            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(000, 0);
            label1.Size = new System.Drawing.Size(35, 13);
            label1.Padding = new System.Windows.Forms.Padding(4);
            
            label1.TabIndex = 1;
            label1.Name = row.label;
            if(header)
                label1.Text = row.label;
            else
                label1.Text = (i-1).ToString() + ". " + row.label;
            
            label2.AutoSize = true;
            label2.Padding = new System.Windows.Forms.Padding(4);
            label2.Location = new System.Drawing.Point(500, 0);
            label2.Size = new System.Drawing.Size(35, 13);
            label2.TabIndex = 1;
            label2.Name = row.value1;
            label2.Text = row.value1;

            label3.AutoSize = true;
            label3.Padding = new System.Windows.Forms.Padding(4);
            label3.Location = new System.Drawing.Point(700, 0);
            label3.Size = new System.Drawing.Size(35, 13);
            label3.TabIndex = 1;
            label3.Name = row.value2;
            label3.Text = row.value2;

            panel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            panel.Padding = new System.Windows.Forms.Padding(4);
            panel.AutoSize = true;
            panel.Padding = new System.Windows.Forms.Padding(0);
            panel.Margin = new System.Windows.Forms.Padding(0);
            panel.Location = new System.Drawing.Point(0, 0);
            panel.Size = new System.Drawing.Size(600, 20);
            panel.TabIndex = 0;
            panel.Name = "panel_" + row.label;

            panel.Controls.Add(label1);
            panel.Controls.Add(label2);
            panel.Controls.Add(label3);

            return panel;
        }
    }
}
