﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Angosat
{
    public partial class Form2 : Form
    {
        public TabPage tab;
        public Boolean isC;
        public Form2(Boolean isC, TabPage tabPage)
        {
            tab = tabPage;
            this.isC = isC;

            InitializeComponent();
            LoadData();

            if (isC)
            {
                this.panel_attenuation_due_to_rain.Hide();
                this.panel_atmospheric_loss.Hide();
            }
        }

        private void LoadData()
        {
            Evaluations evals = Evaluations.getEvals(tab);

            this.input_antenna_angle.Text = evals.s_orientationCoefficient.ToString();
            this.input_attenuation_due_to_rain.Text = evals.s_attenuation_due_to_rain.ToString();
            this.input_operating_frequency.Text = evals.s_operating_frequency.ToString();
            this.input_antenna_diameter.Text = evals.s_antenna_diameter.ToString();
            this.input_station_longitude.Text = evals.s_station_longitude.ToString();
            this.input_satellite_longitude.Text = evals.s_satellite_longitude.ToString();
            this.input_station_latitude.Text = evals.s_station_latitude.ToString();
            this.input_atmospheric_loss.Text = evals.s_atmospheric_loss.ToString();
            this.n_per_czs_input.Text = evals.n_per_czs.ToString();
            this.fec_input.Text = evals.FEC_per_czs.ToString();
            this.Rb_per_czs_input.Text = evals.Rb_per_czs.ToString();
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Evaluations evals = Evaluations.getEvals(tab);
                evals.s_orientationCoefficient = Evaluations.parseDouble(this.input_antenna_angle, this.label_antenna_angle);;
                evals.s_attenuation_due_to_rain = Evaluations.parseDouble(this.input_attenuation_due_to_rain, this.label_station_longitude);
                if (isC)
                    evals.s_operating_frequency = Evaluations.parseDouble(this.input_operating_frequency, this.label_operating_frequency, 3570f, 6355f, true);
                else
                    evals.s_operating_frequency = Evaluations.parseDouble(this.input_operating_frequency, this.label_operating_frequency, 10990f, 14210, true);
                evals.s_antenna_diameter = Evaluations.parseDouble(this.input_antenna_diameter, this.label_antenna_diameter);
                evals.s_satellite_longitude = Evaluations.parseDouble(this.input_satellite_longitude, this.label_satellite_longitude);;
                evals.s_station_longitude = Evaluations.parseDouble(this.input_station_longitude, this.label_station_longitude);
                evals.s_station_latitude = Evaluations.parseDouble(this.input_station_latitude, this.label_station_latitude);
                evals.s_atmospheric_loss = Evaluations.parseDouble(this.input_atmospheric_loss, this.label_atmospheric_loss);
                evals.n_per_czs = Evaluations.parseDouble(this.n_per_czs_input, this.n_per_czs_label);
                evals.FEC_per_czs = Evaluations.parseDouble(this.fec_input, this.fec_label);
                evals.Rb_per_czs = Evaluations.parseDouble(this.Rb_per_czs_input, this.Rb_per_czs_label);
            }
            catch (Exception ex)
            {
                MessageBox.Show("У вас ошибка в поле " + ex.Message,
                    "Исключительная ситуация",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button1);
                e.Cancel = true;
            }
        }
    }
}
