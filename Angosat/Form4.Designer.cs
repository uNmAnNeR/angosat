﻿namespace Angosat
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.input_equivalent_isotropic_radiated_power = new System.Windows.Forms.TextBox();
            this.label_equivalent_isotropic_radiated_power = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // input_equivalent_isotropic_radiated_power
            // 
            this.input_equivalent_isotropic_radiated_power.Location = new System.Drawing.Point(320, 3);
            this.input_equivalent_isotropic_radiated_power.Name = "input_equivalent_isotropic_radiated_power";
            this.input_equivalent_isotropic_radiated_power.Size = new System.Drawing.Size(148, 20);
            this.input_equivalent_isotropic_radiated_power.TabIndex = 8;
            this.input_equivalent_isotropic_radiated_power.Text = "39,82";
            // 
            // label_equivalent_isotropic_radiated_power
            // 
            this.label_equivalent_isotropic_radiated_power.AutoSize = true;
            this.label_equivalent_isotropic_radiated_power.Location = new System.Drawing.Point(6, 7);
            this.label_equivalent_isotropic_radiated_power.Name = "label_equivalent_isotropic_radiated_power";
            this.label_equivalent_isotropic_radiated_power.Size = new System.Drawing.Size(259, 13);
            this.label_equivalent_isotropic_radiated_power.TabIndex = 7;
            this.label_equivalent_isotropic_radiated_power.Text = "Эквивалентная изотропно-излучаемая мощность";
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.label_equivalent_isotropic_radiated_power);
            this.panel1.Controls.Add(this.input_equivalent_isotropic_radiated_power);
            this.panel1.Location = new System.Drawing.Point(8, 8);
            this.panel1.Margin = new System.Windows.Forms.Padding(8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(471, 26);
            this.panel1.TabIndex = 9;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(520, 74);
            this.Controls.Add(this.panel1);
            this.Name = "Form4";
            this.Text = "КА передатчика АФУ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form4_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox input_equivalent_isotropic_radiated_power;
        private System.Windows.Forms.Label label_equivalent_isotropic_radiated_power;
        private System.Windows.Forms.Panel panel1;

    }
}